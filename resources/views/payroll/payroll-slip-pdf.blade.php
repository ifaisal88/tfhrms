<!doctype html>
<html lang="en">
<head>
    <title>:: Lucid HR :: Home</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css" media="all">
        body {
            padding: 0px 15px;
        }

        .full-width {
            width: 100%;
            float: left;
        }

        .siteInfo {
            width: 85%;
            float: left;
        }

        .invoiceNum {
            width: 14%;
            float: left;
            padding-right: 1%;
        }

        .half-width {
            width: 48.5%;
            float: left;
        }

        .allow_deduct {
            width: 100%;
            text-align: left;
        }

        .allow_deduct th,
        .allow_deduct td {
            padding: 15px 25px;
        }

        .allow_deduct thead th {
            color: #fff;
        }

        .allow_deduct td {
            border-bottom: 1px solid #e4e3e3;
            ;
        }

        .allow thead th {
            background-color: #23af46;
        }

        .deduct thead th {
            background-color: #de4848;
        }

    </style>
</head>

<body>
    <div class="full-width" style="padding: 10px 5px 15px 5px;border-bottom: 1px solid #e4e3e3;">
        <div class="siteInfo">
            <p>Replace this with Logo</p>
            <p>
                <b style="line-height: 2;">IML GROUP.</b><br>
                8117 Roosevelt St.<br>
                New Rochelle, NY 10801
            </p>
        </div>
        <div class="invoiceNum" style="text-align: right;">
            <h1>Invoice # 22</h1>
            <p>Salary Month: April 2021</p>
        </div>
    </div>
    <div class="full-width" style="padding:10px 0px;">
        <img src="user.png" alt="" style="float:left;">
        <p>Cleaning Supervisor<br>Employee ID: lkX791sbn3</p>
    </div>
    <div class="full-width" style="padding-bottom: 20px;">
        <div class="half-width" style="padding-right:1.5%;">
            <table class="allow_deduct allow" cellspacing=0>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Allowances</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Basic Salary</td>
                        <td>700</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>House</td>
                        <td>800</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Insentive</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Insentive</td>
                        <td>200</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Total Earnings</th>
                        <th>3300</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="half-width" style="padding-left:1.5%;">
            <table class="allow_deduct deduct" cellspacing=0>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Allowances</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Basic Salary</td>
                        <td>700</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>House</td>
                        <td>800</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Insentive</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Insentive</td>
                        <td>200</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Total Deduction</th>
                        <th>3300</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="full-width">
        <div class="siteInfo">
            <h2 style="margin-bottom: 10px;">Note </h2>
            <p>
                Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web
                designs.
            </p>
        </div>
        <div class="invoiceNum" style="text-align: right;">
            <p style="margin-top: 60px;">
                <b>Total Earnings:</b> 3300<br>
                <b>Total Deductions:</b> 600<br>
            </p>
            <h2>Net Salary 2700</h2>
        </div>
    </div>
</body>

</html>
