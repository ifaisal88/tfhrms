<div id="left-sidebar" class="sidebar">
    <div class="sidebar-scroll">
        <div class="user-account">
            @php
                $path = Storage::disk('media_images')->exists(Auth::user()->employee_id . '/profile.jpg') ? asset('media_images/' . Auth::user()->employee_id . '/profile.jpg') : asset('media_images/profile.jpg');
            @endphp
            <img src="{{ $path }}" class="rounded-circle user-photo" alt="Profile Picture" />
            <div class="dropdown">
                <span>Welcome, </span> <br />
                <strong>{{ Auth::User()->first_name }}</strong>
            </div>
            <hr>
            <div class="row">
                <div class="col-4">
                    <h6>5+</h6>
                    <small>Experience</small>
                </div>
                <div class="col-4">
                    <h6>400+</h6>
                    <small>Employees</small>
                </div>
                <div class="col-4">
                    <h6>80+</h6>
                    <small>Clients</small>
                </div>
            </div>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#hr_menu" id="mainMenuTab">HR</a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#setting" id="settingMenuTab"><i class="icon-settings"></i></a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content p-l-0 p-r-0">
            <div class="tab-pane animated fadeIn active" id="hr_menu">
                <nav class="sidebar-nav">
                    <ul class="main-menu metismenu">
                        <li>
                            <a href="{{ route('dashboard') }}"><i class="icon-speedometer"></i><span>Dashboard</span></a>
                        </li>
                        <li>
                            <a href="#Employees" class="has-arrow"><i class="icon-users"></i><span>Employees</span></a>
                            <ul>
                                <li><a href="{{ route('employees.index') }}">All Employees</a></li>
                                <li><a href="{{ route('linemanagers.read.index') }}">Line Manager</a></li>
                                <li><a href="{{ route('employeeLeaves.store.view') }}">Leave Requests</a></li>
                                <li><a href="{{ route('employeeLeaves.store.perosnalLeaveView') }}">Personal Leave</a></li>
                                <li><a href="{{ route('employeeAttendances.store.view') }}">Attendance</a></li>
                                <li><a href="{{ route('employeeAttendances.read.history') }}">Attendance History</a></li>
                                <li><a href="emp-departments.html">Departments</a></li>
                            </ul>
                        </li>
                        <li><a href="{{ route('eventDetails.read.index') }}"><i class="icon-calendar"></i>Calendar</a></li>
                        
                        <li>
                            <a href="#Payroll" class="has-arrow"><i
                                    class="icon-credit-card"></i><span>Payroll</span></a>
                            <ul>
                                <li><a href="{{ route('payrolls.read.salary') }}">Generate Salary</a></li>
                                <li><a href="{{ route('payrolls.read.salaryDetails') }}">Salary Details</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="tab-pane animated fadeIn" id="setting">
                <nav class="sidebar-nav">
                    <ul class="main-menu metismenu">
                        <li>
                            <a href="#" class="has-arrow"><i class="icon-briefcase"></i> <span>Assets</span></a>
                            <ul>
                                <li><a href="{{ route('assetBrands.read.index') }}">Brands</a></li>
                                <li><a href="{{ route('assetsCondition.read.index') }}">Condition</a></li>
                                <li><a href="{{ route('assetsTypes.read.index') }}">Types</a></li>
                                <li><a href="{{ route('assetVendors.read.index') }}">Vendors</a></li>
                                <li><a href="{{ route('assetItems.read.index') }}">Items</a></li>
                            </ul>
                        </li>
                        <!-- <li>
                            <a href="#" class="has-arrow"><i class="icon-briefcase"></i> <span>Insurance</span></a>
                            <ul>
                                <li><a href="{{ route('insuranceComapnies.read.index') }}">Companies</a></li>
                                <li><a href="{{ route('insuranceCategories.read.index') }}">Categories</a></li>
                                <li><a href="{{ route('insuranceProviders.read.index') }}">Providers</a></li>
                                <li><a href="{{ route('insuranceBenefits.read.index') }}">Benefits</a></li>
                            </ul>
                        </li> -->
                        <li>
                            <a href="#" class="has-arrow"><i class="icon-briefcase"></i> <span>Education</span></a>
                            <ul>
                                <li><a href="{{ route('institutes.read.index') }}">Institutes</a></li>
                                <li><a href="{{ route('qualifications.read.index') }}">Qualifications</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="has-arrow"><i class="icon-briefcase"></i> <span>Payments</span></a>
                            <ul>
                                <li><a href="{{ route('paygroups.read.index') }}">Pay Groups</a></li>
                                <li><a href="{{ route('paymentTypes.read.index') }}">Payment Type</a></li>
                                <li><a href="{{ route('salaryRules.read.index') }}">Salary Rule</a></li>
                                <li><a href="{{ route('allowances.read.index') }}">Allowances</a></li>
                                <li><a href="{{ route('deductables.read.index') }}">Deductables</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="has-arrow"><i class="icon-briefcase"></i> <span>TF Management</span></a>
                            <ul style="height:400px; overflow-y: scroll;">
                                <li><a href="{{ route('attendanceStatuses.read.index') }}">Attendance Status</a></li>
                                <li><a href="{{ route('banks.read.index') }}">Employee Bank Details</a></li>
                                <li><a href="{{ route('cities.read.index') }}">Cities</a></li>
                                <li><a href="{{ route('companies.read.index') }}">Organizations</a></li>
                                <li><a href="{{ route('accounts.read.index') }}">TF Bank Accounts</a></li>
                                <li><a href="{{ route('contractTypes.read.index') }}">Contract Types</a></li>
                                <!-- <li><a href="{{ route('costCenters.read.index') }}">Cost Center</a></li> -->
                                <li><a href="{{ route('countries.read.index') }}">Countries</a></li>
                                <li><a href="{{ route('currencies.read.index') }}">Currencies</a></li>
                                <li><a href="{{ route('departments.read.index') }}">Departments</a></li>
                                <li><a href="{{ route('designations.read.index') }}">Designations</a></li>
                                <li><a href="{{ route('documentStatues.read.index') }}">Document Status</a></li>
                                <li><a href="{{ route('documentTypes.read.index') }}">Document Types</a></li>
                                <li><a href="{{ route('employeeCategories.read.index') }}">Employee Categories</a></li>
                                <li><a href="{{ route('eventTypes.read.index') }}">Calendar Types</a></li>
                                <li><a href="{{ route('eventDetails.read.index') }}">Calendar</a></li>
                                <li><a href="{{ route('employeeStatues.read.index') }}">Employee Status</a></li>
                                <!-- <li><a href="{{ route('employeeTypes.read.index') }}">Employee Type</a></li> -->
                                <li><a href="{{ route('gratuityRules.read.index') }}">Gratuity Rules</a></li>
                                <li><a href="{{ route('shifts.read.index') }}">Job Shifts</a></li>
                                <li><a href="{{ route('leavePerAnnums.read.index') }}">Leave Entitlement</a></li>
                                <li><a href="{{ route('leaveTypes.read.index') }}">Leave Types</a></li>
                                <!-- <li><a href="{{ route('positionCategories.read.index') }}">Position Categories</a></li> -->
                                <li><a href="{{ route('projects.read.index') }}">Projects</a></li>
                                <li><a href="{{ route('relations.read.index') }}">Relations</a></li>
                                <li><a href="{{ route('religions.read.index') }}">Religions</a></li>
                                <li><a href="{{ route('routes.read.index') }}">Routes</a></li>
                                <!-- <li><a href="{{ route('sponsors.read.index') }}">Sponsors</a></li> -->
                                <li><a href="{{ route('subDepartments.read.index') }}">Sub Departments</a></li>
                                <li><a href="{{ route('ticketEntitlements.read.index') }}">Ticket Entitlement</a></li>
                                <!-- <li><a href="{{ route('visaStatues.read.index') }}">Visa Status</a></li> -->
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="has-arrow"><i class="icon-briefcase"></i> <span>RBAC</span></a>
                            <ul>
                                <li><a href="{{ route('roles.read.index') }}">Roles</a></li>
                                <li><a href="{{ route('permissions.read.index') }}">Permissions</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>