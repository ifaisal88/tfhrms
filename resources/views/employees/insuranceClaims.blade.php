@extends('layouts.templates')
@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i lass="fa fa-arrow-left"></i></a> Employee Insurance Claim</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item">Employee</li>
                        <li class="breadcrumb-item active">Insurance Claim</li>
                    </ul>
                </div>
            </div>
        </div>

        <div id="app">
            <employee-insurance-claims :employee-id="'{{\Auth::user()->employee_id}}'"></employee-insurance-claims>
        </div>
    </div>
</div>
@include('payroll.modals.load-employees')


@endsection
