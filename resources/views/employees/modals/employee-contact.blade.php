<form action="{{ route('employees.update.contactDetails', [$employee->employee_id]) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="modal fade" id="employee-contact" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Employee Contact Details</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-md-6 pull-left">
                        <label for="email">Company Email</label>
                        <input type="text" class="form-control @error('email') is-invalid @enderror" name="email" id="email" placeholder="Company Email" value="{{ old('email', $employee->email) }}">
                        @error('email')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-6 pull-left">
                        <label for="personal_email">Personal Email</label>
                        <input type="text" class="form-control @error('personal_email') is-invalid @enderror" name="personal_email" id="personal_email" placeholder="Personal Email" value="{{ old('personal_email', $employee->personal_email) }}">
                        @error('personal_email')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-4 pull-left">
                        <label for="mobile_number">Mobile Number</label>
                        <input type="text" class="form-control @error('mobile_number') is-invalid @enderror" name="mobile_number" id="mobile_number" placeholder="Mobile Number" value="{{ old('mobile_number', $employee->mobile_number) }}">
                        @error('mobile_number')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-4 pull-left">
                        <label for="home_phone_number">Home Phone</label>
                        <input type="text" class="form-control @error('home_phone_number') is-invalid @enderror" name="home_phone_number" id="home_phone_number" placeholder="Home Phone" value="{{ old('home_phone_number', $employee->home_phone_number) }}">
                        @error('home_phone_number')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-4 pull-left">
                        <label for="work_mobile">Work Number</label>
                        <input type="text" class="form-control @error('work_mobile') is-invalid @enderror" name="work_mobile" id="work_mobile" placeholder="Work Number" value="{{ old('work_mobile', $employee->work_mobile) }}">
                        @error('work_mobile')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="current_address">Current Address</label>
                        <textarea name="current_address" id="current_address" class="form-control @error('current_address') is-invalid @enderror">{{($employee->current_address == '')? '' : $employee->current_address }}</textarea>
                        @error('current_address')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="home_country_address">Home Country Address</label>
                        <textarea name="home_country_address" id="home_country_address" class="form-control @error('home_country_address') is-invalid @enderror">{{ old('home_country_address', $employee->home_country_address) }}</textarea>
                        @error('home_country_address')
                            <span class="text-danger"><small>{{ $message }}</small></span>
                        @enderror
                    </div>
			    </div>                
                <div class="modal-footer" id="modal-footer">
                    <div id="dependant_div">
                        <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
		    </div>
        </div>
    </div>
</form>