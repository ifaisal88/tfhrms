<form action="{{ route('employees.store.documents', [$employee->employee_id]) }}" method="post" enctype='multipart/form-data'>
    @csrf
    <div class="modal fade" id="employee-documents" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="smallModalLabel">Employee Documents</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-12 pull-left">
                        <label for="document_number">Document Number</label>
                        <input type="text" name="document_number" id="document_number" class="form-control" placeholder="Document Number">
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="document_status_id">Document Status</label>
                        <select name="document_status_id" id="document_status_id" class="simple-select2 w-100">
                            <option value="">Document Status</option>
                            @foreach($document_statuses as $document_status)
                                <option value="{{ $document_status->id }}">{{ $document_status->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="document_type_id">Document Type</label>
                        <select name="document_type_id" id="document_type_id" class="simple-select2 w-100">
                            <option value="">Document Type</option>
                            @foreach($document_types as $document_type)
                                <option value="{{ $document_type->id }}">{{ $document_type->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left"">
                        <label for="issue_date">Issue date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="issue_date" id="issue_date" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Issue date">
                        </div>
                    </div>
                    <div class="form-group col-6 pull-left"">
                        <label for="expiry_date">Expiry date</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="icon-calendar"></i></span>
                            </div>
                            <input name="expiry_date" id="expiry_date" data-provide="datepicker" data-date-autoclose="true" class="form-control" data-date-format="yyyy-mm-dd" placeholder="Expiry date">
                        </div>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="place_of_issue_country_id">Place of Issue</label>
                        <select name="place_of_issue_country_id" id="place_of_issue_country_id" class="simple-select2 w-100">
                            <option value="">Place of Issue</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-6 pull-left">
                        <label for="issued_by_country_id">Issued By</label>
                        <select name="issued_by_country_id" id="issued_by_country_id" class="simple-select2 w-100">
                            <option value="">Issued By</option>
                            @foreach($countries as $country)
                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="notify_period_in_days">Notify (days)</label>
                        <input type="text" name="notify_period_in_days" id="notify_period_in_days" class="form-control" placeholder="Notify (days)">
                    </div>
                    <div class="form-group col-md-12 pull-left">
                        <label for="description">Description</label>
                        <textarea name="description" id="description" class="form-control"></textarea>
                    </div>
                    <div class="form-group col-12 pull-left">
                        <label for="document_path">Document</label>
                        <input type="file" name="document_path" id="document_path" class="dropify">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                </div>
            </div>
        </div>
    </div>
</form>