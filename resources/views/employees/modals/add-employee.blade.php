<form action="{{ route('employees.store') }}" method="POST">
    <div class="modal fade" id="add-employee" tabindex="-1" role="dialog">
        @csrf
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Add Contact</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-4 pull-left">
                            <label for="first_name">First Name *</label>
                            <input type="text" name="first_name" id="first_name" class="form-control @error('first_name') is-invalid @enderror" value="{{ old('first_name') }}" placeholder="First Name">
                            @error('first_name')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="form-group col-4 pull-left">
                            <label for="middle_name">Middle Name</label>
                            <input type="text" name="middle_name" id="middle_name" class="form-control @error('middle_name') is-invalid @enderror" value="{{ old('middle_name') }}" placeholder="Middle Name">
                        </div>
                        <div class="form-group col-4 pull-left">
                            <label for="last_name">last Name</label>
                            <input type="text" name="last_name" id="last_name" class="form-control @error('last_name') is-invalid @enderror" value="{{ old('last_name') }}" placeholder="Last Name">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6 pull-left">
                            <label for="email">Company Email *</label>
                            <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Email Address">
                            @error('email')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="form-group col-6 pull-left">
                            <label for="last_name">Home Phone Number *</label>
                            <input type="text" name="home_phone_number" id="home_phone_number" class="form-control @error('home_phone_number') is-invalid @enderror" value="{{ old('home_phone_number') }}" placeholder="Home Phone Number">
                            @error('home_phone_number')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div> 
                    </div>
                    <div class="row">
                        <div class="form-group col-6 pull-left">
                            <label for="emirates_id">Emirates ID *</label>
                            <input type="text" name="emirates_id" id="emirates_id" class="form-control @error('emirates_id') is-invalid @enderror" value="{{ old('emirates_id') }}" placeholder="Emirates ID">
                            @error('emirates_id')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                        <div class="form-group col-6 pull-left">
                            <label for="personal_id">Personal ID *</label>
                            <input type="text" name="personal_id" id="personal_id" class="form-control @error('personal_id') is-invalid @enderror" value="{{ old('personal_id') }}" placeholder="Personal ID">
                            @error('personal_id')
                                <span class="text-danger"><small>{{ $message }}</small></span>
                            @enderror
                        </div>
                    </div>
                    <div class="row">                    
                        <div class="form-group col-6 pull-left">
                            <div class="form-check form-check-inline">
                                <label class="toggle">
                                    <input type="radio" name="status" id="status" value="1" checked> <span class="label-text">Active</span>
                                </label>
                            </div>
                            <div class="form-check form-check-inline">
                                <label class="toggle">
                                    <input type="radio" name="status" id="status" value="0"> <span class="label-text">In-Active</span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" id="modal-footer">
                    <div id="dependant_div">
                        <button type="submit" class="btn btn-primary">SAVE CHANGES</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>