@extends('layouts.templates')
@section('content')
<div id="main-content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <h2><a href="javascript:void(0);" class="btn btn-xs btn-link btn-toggle-fullwidth"><i lass="fa fa-arrow-left"></i></a> Cities/States</h2>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('dashboard') }}"><i class="icon-home"></i></a></li>
                        <li class="breadcrumb-item">Settings</li>
                        <li class="breadcrumb-item active">Company Management</li>
                    </ul>
                </div>
            </div>
        </div>

        <div id="app">
            <isnurance-benefits-main></isnurance-benefits-main>
        </div>
    </div>
</div>
@endsection