<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\AllowanceController;
use App\Http\Controllers\AssetBrandController;
use App\Http\Controllers\AssetConditionController;
use App\Http\Controllers\AssetItemController;
use App\Http\Controllers\AssetItemStockController;
use App\Http\Controllers\AssetTypeController;
use App\Http\Controllers\AssetVendorController;
use App\Http\Controllers\AttendanceStatusController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\CityController;
use App\Http\Controllers\CompanyController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\EmployeeDependantController;
use App\Http\Controllers\ContactPersonController;
use App\Http\Controllers\CostCenterController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\CurrencyController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\DesignationController;
use App\Http\Controllers\DocumentStatusController;
use App\Http\Controllers\DocumentTypeController;
use App\Http\Controllers\InstituteController;
use App\Http\Controllers\InsuranceCategoryController;
use App\Http\Controllers\InsuranceCompanyController;
use App\Http\Controllers\PayGroupController;
use App\Http\Controllers\PositionCategoryController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\QualificationController;
use App\Http\Controllers\RelationController;
use App\Http\Controllers\ReligionController;
use App\Http\Controllers\RouteController;
use App\Http\Controllers\SponsorController;
use App\Http\Controllers\SubDepartmentController;
use App\Http\Controllers\TicketEntitlementController;
use App\Http\Controllers\ContractTypeController;
use App\Http\Controllers\EmployeeAssetController;
use App\Http\Controllers\EmployeeBankDetailController;
use App\Http\Controllers\EmployeeCategoryController;
use App\Http\Controllers\EmployeeDependantBenefitController;
use App\Http\Controllers\EmployeeDocumentController;
use App\Http\Controllers\EmployeeEducationController;
use App\Http\Controllers\EmployeeStatusController;
use App\Http\Controllers\EmployeeTypeController;
use App\Http\Controllers\InsuranceProviderController;
use App\Http\Controllers\ShiftController;
use App\Http\Controllers\LeavePerAnnumController;
use App\Http\Controllers\PaymentTypeController;
use App\Http\Controllers\SalaryRuleController;
use App\Http\Controllers\VisaStatusController;
use App\Http\Controllers\EmployeeSalaryDetailController;
use App\Http\Controllers\DeductionController;
use App\Http\Controllers\EmployeeAssetImageController;
use App\Http\Controllers\EmployeeAttendanceController;
use App\Http\Controllers\EmployeeLeaveController;
use App\Http\Controllers\EmployeeSalaryController;
use App\Http\Controllers\EventDetailController;
use App\Http\Controllers\EventTypeController;
use App\Http\Controllers\GratuityRuleController;
use App\Http\Controllers\InsuranceBenefitsController;
use App\Http\Controllers\InsuranceClaimController;
use App\Http\Controllers\LeaveTypeController;
use App\Http\Controllers\LineManagerController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\RoleController;
use App\Http\Middleware\UserAccess;
use App\Http\Requests\EmployeeDocumentRequest;


require __DIR__ . '/auth.php';

Route::get('/test', function () {
    return view('test');
})->name('test');

Route::group(['prefix' => '/salaryRules', 'as' => 'salaryRules.'], function () {
    Route::get('/', [SalaryRuleController::class, 'index'])->name('read.index');
    Route::post('/', [SalaryRuleController::class, 'store'])->name('store');
    Route::put('/{salaryRule}', [SalaryRuleController::class, 'update'])->name('update');
    Route::get('/all/data', [SalaryRuleController::class, 'allSalaryRule'])->name('read.all');
});

Route::post('paygroups/store', [EmployeeSalaryDetailController::class, 'storeSalary'])->name('payroll.store.salary');


Route::middleware(['auth'])->group(function () {
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    // Route::get('/test', [UserController::class, 'test'])->name('test');

    Route::resource('/dependants', EmployeeDependantController::class);
    Route::resource('/contactPerson', ContactPersonController::class);

    Route::resource('/employees', EmployeeController::class);
    Route::group(['prefix' => 'employees', 'as' => 'employees.'], function () {
        Route::post('/{id}/contact-person', [EmployeeController::class, 'storeContactPerson'])->name('store.contactPerson');
        Route::post('/{id}/dependants', [EmployeeController::class, 'storeDependants'])->name('store.dependants');
        Route::post('/{user}/education', [EmployeeController::class, 'storeEducation'])->name('store.educationDetail');
        Route::post('/{id}/bank-details', [EmployeeController::class, 'storeAccountDetails'])->name('store.accountDetails');
        Route::post('/{id}/documents', [EmployeeController::class, 'storeDocuments'])->name('store.documents');
        Route::post('/{id}/assets', [EmployeeController::class, 'storeAssets'])->name('store.assets');
        Route::post('/{id}/dependant-benefits', [EmployeeController::class, 'storeDependantBenefits'])->name('store.dependantBenefits');
        Route::get('/{user}/profile-info', [EmployeeController::class, 'profile'])->name('read.profile');
        Route::get('/{id}/basic-info', [EmployeeController::class, 'basicInfo'])->name('read.basicInfo');
        Route::get('/{id}/offical-info', [EmployeeController::class, 'officalInfo'])->name('read.officalInfo');
        Route::put('/{id}/main-info', [EmployeeController::class, 'updateEmployeeMainInfo'])->name('update.mainInfo');
        Route::put('/{id}/personal-details', [EmployeeController::class, 'updateEmployeePersonalDetails'])->name('update.personalDetails');
        Route::put('/{user}/employee-contact', [EmployeeController::class, 'updateEmployeeContactDetails'])->name('update.contactDetails');
        Route::put('/{id}/employee-official-details', [EmployeeController::class, 'updateEmployeeOfficialDetails'])->name('update.officialDetails');
        Route::post('/{id}/employee-profile-image', [EmployeeController::class, 'updateProfileImage'])->name('update.profileImage');
        Route::put('/{id}/employee-rules', [EmployeeController::class, 'updateEmployeeRules'])->name('update.employeeRules');
        Route::put('/{user}/employee-pay-elements', [EmployeeController::class, 'updateEmployeePayElements'])->name('update.payElements');
        Route::put('/{user}/employee-insurance-details', [EmployeeController::class, 'updateInsuranceDetails'])->name('update.insuranceDetails');
        Route::get('/all/data', [EmployeeController::class, 'allEmployees'])->name('read.all');
        Route::get('/all/un-assigned', [EmployeeController::class, 'unassigned'])->name('read.unassignedEmployees');
        Route::get('/{id}/dependents', [EmployeeController::class, 'dependents'])->name('read.all');
        Route::get('/{id}/show', [EmployeeController::class, 'allDetails'])->name('read.allDetails');
        Route::get('/all/leaves', [EmployeeController::class, 'leaveEmployees'])->name('read.leaveEmployees');
        Route::get('/attendance/excel', [EmployeeController::class, 'attendanceSheetDownload'])->name('read.attendanceSheet');
        Route::get('/{employee}/employees', [EmployeeController::class, 'lineManagerEmployees'])->name('read.lineManagerEmployee');
        Route::patch('/unset/line-manager', [EmployeeController::class, 'unsetLinemanager'])->name('update.linemanager');
    });


    Route::group(['prefix' => 'payrolls', 'as' => 'payrolls.'], function () {
        Route::get('/profile-info', [EmployeeSalaryDetailController::class, 'index'])->name('read.salary');
        Route::get('/salary-details', [EmployeeSalaryDetailController::class, 'details'])->name('read.salaryDetails');
        Route::get('/all/data', [EmployeeSalaryDetailController::class, 'allPayRolles'])->name('read.payroll');
        Route::get('/{id}/payslip', [EmployeeSalaryDetailController::class, 'salarySlip'])->name('read.salarySlip');
    });

    Route::group(['prefix' => 'dependents', 'as' => 'dependents.'], function () {
        Route::put('/{employeeDependant}', [EmployeeDependantController::class, 'update'])->name('update');
    });

    Route::group(['prefix' => 'emergency-contacts', 'as' => 'emergencyContacts.'], function () {
        Route::put('/{contactPerson}', [ContactPersonController::class, 'update'])->name('update');
    });

    Route::group(['prefix' => 'employee-educations', 'as' => 'employeeEducations.'], function () {
        Route::post('/{employeeEducation}/update', [EmployeeEducationController::class, 'update'])->name('update');
        Route::get('/{employeeEducation}/download', [EmployeeEducationController::class, 'download'])->name('read.download');
    });

    Route::group(['prefix' => 'dependet-benefits', 'as' => 'dependetBenefits.'], function () {
        Route::put('/{employeeDependantBenefit}', [EmployeeDependantBenefitController::class, 'update'])->name('update');
        Route::get('/{employeeDependantBenefit}', [EmployeeDependantBenefitController::class, 'show'])->name('read');
    });

    Route::group(['prefix' => 'bank-details', 'as' => 'banks.'], function () {
        Route::put('/{employeeBankDetail}', [EmployeeBankDetailController::class, 'update'])->name('update.banks');
    });

    Route::group(['prefix' => 'employee-documents', 'as' => 'employeeDocuments.'], function () {
        Route::post('/{employeeDocument}/update', [EmployeeDocumentController::class, 'update'])->name('update.documents');
    });

    Route::group(['prefix' => 'employee-assets', 'as' => 'employeeAssets.'], function () {
        Route::get('/{employeeAsset}/delete-image', [EmployeeAssetController::class, 'deleteImages'])->name('delete.images');
        Route::put('/{employeeAsset}/update-image', [EmployeeAssetController::class, 'updateImages'])->name('update.images');
        Route::put('/{employeeAsset}', [EmployeeAssetController::class, 'update'])->name('update.assets');
    });

    Route::group(['prefix' => 'employee-asset-images', 'as' => 'employeeAssetImages.'], function () {
        Route::put('/{employeeAssetImage}', [EmployeeAssetImageController::class, 'update'])->name('update');
        Route::delete('/{employeeAssetImage}', [EmployeeAssetImageController::class, 'destroy'])->name('delete');
    });

    Route::group(['prefix' => 'linemanagers', 'as' => 'linemanagers.'], function () {
        Route::get('/', [LineManagerController::class, 'index'])->name('read.index');
        Route::get('/all/data', [LineManagerController::class, 'allLineManagers'])->name('read.all');
        Route::get('/{linemanager}/employees', [LineManagerController::class, 'employees'])->name('read.employees');
        Route::patch('/{linemanager}/employees', [LineManagerController::class, 'setEmployees'])->name('read.employees');
    });

    Route::group(['prefix' => 'employee-salaries', 'as' => 'employeeSalaries.'], function () {
        Route::put('/{employeeSalary}', [EmployeeSalaryController::class, 'update'])->name('update.employeeSalaries');
        Route::get('/{employeeSalary}/show', [EmployeeSalaryController::class, 'slip'])->name('read.employeeSalaries');
        Route::get('/{employeeSalary}/pdf', [EmployeeSalaryController::class, 'slipPdf'])->name('read.employeeSalariesPdf');
        Route::get('/excel-export', [EmployeeSalaryController::class, 'export'])->name('read.export');
        Route::get('/{employeeSalary}', [EmployeeSalaryController::class, 'show'])->name('read.employeeSalaries');
        Route::get('/export/allowance-template', [EmployeeSalaryController::class, 'allowanceTemplateDownload'])->name('read.allowanceTemplateDownload');
        Route::post('/import/allowance-template', [EmployeeSalaryController::class, 'allowanceTemplateUpload'])->name('read.allowanceTemplateUpload');
        Route::get('/export/deductable-template', [EmployeeSalaryController::class, 'deductableTemplateDownload'])->name('read.allowanceTemplateDownload');
        Route::post('/import/deductable-template', [EmployeeSalaryController::class, 'deductableTemplateUpload'])->name('read.allowanceTemplateUpload');
    });

    Route::group(['prefix' => 'employee-attendances', 'as' => 'employeeAttendances.'], function () {
        Route::get('/', function () {
            return view('employees.attendance');
        })->name('store.view');
        Route::get('/history', function () {
            return view('employees.attendance-history');
        })->name('read.history');
        Route::get('/history-data', [EmployeeAttendanceController::class, 'history'])->name('read.historyData');
        Route::get('/export', [EmployeeAttendanceController::class, 'export'])->name('read.export');
        Route::post('/', [EmployeeAttendanceController::class, 'store'])->name('store');
        Route::put('/{employeeAttendance}', [EmployeeAttendanceController::class, 'update'])->name('update');
        Route::post('/import', [EmployeeAttendanceController::class, 'import'])->name('add.attendanceSheet');
    });

    Route::group(['prefix' => 'employee-leaves', 'as' => 'employeeLeaves.'], function () {
        Route::get('/', function () {
            return view('employees.leave');
        })->name('store.view');
        Route::get('/personal', function () {
            return view('employees.leavePersonal');
        })->name('store.perosnalLeaveView');
        Route::post('/', [EmployeeLeaveController::class, 'updateCreate'])->name('update');
    });

    Route::group(['prefix' => 'insurance-claims', 'as' => 'insuranceClaims.'], function () {
        Route::group([], function () {
            Route::get('/', function () {
                return view('employees.insuranceClaims');
            })->name('read.index');
            Route::post('/', [InsuranceClaimController::class, 'store'])->name('store');
            Route::put('/{insuranceClaim}', [InsuranceClaimController::class, 'update'])->name('store');
        });
        Route::get('/all/data', [InsuranceClaimController::class, 'index'])->name('read');
    });

    Route::group(['prefix' => 'asset-brands', 'as' => 'assetBrands.'], function () {
        Route::group([], function () {
            Route::get('/', [AssetBrandController::class, 'index'])->name('read.index');
            Route::post('/', [AssetBrandController::class, 'store'])->name('store.index');
            Route::put('/{assetBrand}', [AssetBrandController::class, 'update'])->name('update.index');
        });
        Route::get('/all/data', [AssetBrandController::class, 'allBrands'])->name('read.all');
    });

    Route::group(['prefix' => 'asset-conditions', 'as' => 'assetsCondition.'], function () {
        Route::group([], function () {
            Route::get('/', [AssetConditionController::class, 'index'])->name('read.index');
            Route::post('/', [AssetConditionController::class, 'store'])->name('store');
            Route::put('/{assetsCondition}', [AssetConditionController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [AssetConditionController::class, 'allConditions'])->name('read.all');
    });

    Route::group(['prefix' => 'asset-types', 'as' => 'assetsTypes.'], function () {
        Route::group([], function () {
            Route::get('/', [AssetTypeController::class, 'index'])->name('read.index');
            Route::post('/', [AssetTypeController::class, 'store'])->name('store');
            Route::put('/{assetType}', [AssetTypeController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [AssetTypeController::class, 'allAssetTypes'])->name('read.all');
    });

    Route::group(['prefix' => 'asset-vendors', 'as' => 'assetVendors.'], function () {
        Route::group([], function () {
            Route::get('/', [AssetVendorController::class, 'index'])->name('read.index');
            Route::post('/', [AssetVendorController::class, 'store'])->name('store');
            Route::put('/{assetVendor}', [AssetVendorController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [AssetVendorController::class, 'allVendors'])->name('read.all');
    });

    Route::group(['prefix' => 'asset-items', 'as' => 'assetItems.'], function () {
        Route::group([], function () {
            Route::get('/', [AssetItemController::class, 'index'])->name('read.index');
            Route::post('/', [AssetItemController::class, 'store'])->name('store');
            Route::put('/{assetItem}', [AssetItemController::class, 'update'])->name('update');
        });
        Route::get('/filter', [AssetItemController::class, 'filter'])->name('read.filter');
        Route::get('/all/data', [AssetItemController::class, 'allItems'])->name('read.all');
    });

    Route::group(['prefix' => 'asset-item-stocks', 'as' => 'assetItemStocks.'], function () {
        Route::group([], function () {
            Route::post('/', [AssetItemStockController::class, 'store'])->name('store');
            Route::put('/{assetItemStock}', [AssetItemStockController::class, 'update'])->name('update');
        });
    });

    Route::group(['prefix' => 'insurance-comapnies', 'as' => 'insuranceComapnies.'], function () {
        Route::group([], function () {
            Route::get('/', [InsuranceCompanyController::class, 'index'])->name('read.index');
            Route::post('/', [InsuranceCompanyController::class, 'store'])->name('store');
            Route::put('/{insuranceCompany}', [InsuranceCompanyController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [InsuranceCompanyController::class, 'allInsuranceComapnies'])->name('read.all');
    });

    Route::group(['prefix' => 'insurance-categories', 'as' => 'insuranceCategories.'], function () {
        Route::group([], function () {
            Route::get('/', [InsuranceCategoryController::class, 'index'])->name('read.index');
            Route::post('/', [InsuranceCategoryController::class, 'store'])->name('store');
            Route::put('/{insuranceCategory}', [InsuranceCategoryController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [InsuranceCategoryController::class, 'allInsuranceCategories'])->name('read.all');
    });

    Route::group(['prefix' => 'insurance-providers', 'as' => 'insuranceProviders.'], function () {
        Route::group([], function () {
            Route::get('/', [InsuranceProviderController::class, 'index'])->name('read.index');
            Route::get('/all/data', [InsuranceProviderController::class, 'allInsuranceProvider'])->name('read.all');
            Route::post('/', [InsuranceProviderController::class, 'store'])->name('store');
        });
        Route::put('/{insuranceProvider}', [InsuranceProviderController::class, 'update'])->name('update');
    });

    Route::group(['prefix' => 'insurance-benefits', 'as' => 'insuranceBenefits.'], function () {
        Route::group([], function () {
            Route::put('/{insuranceBenefit}', [InsuranceBenefitsController::class, 'update'])->name('update');
            Route::get('/', function () {
                return view('settings.insuranceBenefits.insuranceBenefits');
            })->name('read.index');
            Route::post('/', [InsuranceBenefitsController::class, 'store'])->name('read.index');
        });
        Route::get('/all/data', [InsuranceBenefitsController::class, 'index'])->name('read');
    });

    Route::group(['prefix' => 'insurance', 'as' => 'insurance.'], function () {
        Route::group([], function () {
            Route::get('/employees', function () {
                return view('employees.insurance');
            })->name('read.index');
        });
        Route::get('/employees/all/data', [EmployeeController::class, 'insuranceEmployees'])->name('read.employees');
    });

    Route::group(['prefix' => 'institutes', 'as' => 'institutes.'], function () {
        Route::group([], function () {
            Route::get('/', [InstituteController::class, 'index'])->name('read.index');
            Route::post('/', [InstituteController::class, 'store'])->name('store');
            Route::put('/{institute}', [InstituteController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [InstituteController::class, 'allInstitutes'])->name('read.all');
    });

    Route::group(['prefix' => 'qualifications', 'as' => 'qualifications.'], function () {
        Route::group([], function () {
            Route::get('/', [QualificationController::class, 'index'])->name('read.index');
            Route::post('/', [QualificationController::class, 'store'])->name('store');
            Route::put('/{qualification}', [QualificationController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [QualificationController::class, 'allQualifications'])->name('read.all');
    });

    Route::group(['prefix' => 'paygroups', 'as' => 'paygroups.'], function () {
        Route::group([], function () {
            Route::get('/', [PayGroupController::class, 'index'])->name('read.index');
            Route::post('/', [PayGroupController::class, 'store'])->name('store');
            Route::put('/{payGroup}', [PayGroupController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [PayGroupController::class, 'allPaygroups'])->name('read.all');
        Route::get('/{payGroup}/employees', [PayGroupController::class, 'loadEmployees'])->name('read.employees');
        Route::get('/{payGroup}/salary-employees', [PayGroupController::class, 'salaryEmployees'])->name('read.salaryEmployees');
        Route::get('/{payGroup}/salary-generated-employees', [PayGroupController::class, 'salaryGeneratedEmployees'])->name('read.salaryGeneratedEmployees');
    });
    Route::group(['prefix' => 'payment-types', 'as' => 'paymentTypes.'], function () {
        Route::group([], function () {
            Route::get('/', [PaymentTypeController::class, 'index'])->name('read.index');
            Route::post('/', [PaymentTypeController::class, 'store'])->name('store');
            Route::put('/{paymentType}', [PaymentTypeController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [PaymentTypeController::class, 'allPaymentType'])->name('read.all');
    });

    Route::group(['prefix' => 'salary-rules', 'as' => 'salaryRules.'], function () {
        Route::group([], function () {
            Route::post('/', [SalaryRuleController::class, 'store'])->name('store');
            Route::put('/{salaryRule}', [SalaryRuleController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [SalaryRuleController::class, 'allSalaryRule'])->name('read.all');
    });

    Route::group(['prefix' => 'allowances', 'as' => 'allowances.'], function () {
        Route::group([], function () {
            Route::get('/', [AllowanceController::class, 'index'])->name('read.index');
            Route::post('/', [AllowanceController::class, 'store'])->name('store');
            Route::put('/{allowance}', [AllowanceController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [AllowanceController::class, 'allAllowances'])->name('read.all');
        Route::get('/all/default', [AllowanceController::class, 'allDefaultAllowances'])->name('read.all');
    });

    Route::group(['prefix' => 'deductables', 'as' => 'deductables.'], function () {
        Route::group([], function () {
            Route::get('/', [DeductionController::class, 'index'])->name('read.index');
            Route::post('/', [DeductionController::class, 'store'])->name('store');
            Route::put('/{deduction}', [DeductionController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [DeductionController::class, 'allDeductions'])->name('read.all');
    });

    Route::group(['prefix' => 'attendance-statuses', 'as' => 'attendanceStatuses.'], function () {
        Route::group([], function () {
            Route::get('/', [AttendanceStatusController::class, 'index'])->name('read.index');
            Route::post('/', [AttendanceStatusController::class, 'store'])->name('store');
            Route::put('/{attendanceStatus}', [AttendanceStatusController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [AttendanceStatusController::class, 'allStatuses'])->name('read.all');
    });

    Route::group(['prefix' => 'banks', 'as' => 'banks.'], function () {
        Route::group([], function () {
            Route::get('/', [BankController::class, 'index'])->name('read.index');
            Route::post('/', [BankController::class, 'store'])->name('store');
            Route::put('/{bank}', [BankController::class, 'update'])->name('update.banks');
        });
        Route::get('/all/data', [BankController::class, 'allBanks'])->name('read.all');
    });

    Route::group(['prefix' => 'cities', 'as' => 'cities.'], function () {
        Route::group([], function () {
            Route::get('/', [CityController::class, 'index'])->name('read.index');
            Route::post('/', [CityController::class, 'store'])->name('store');
            Route::put('/{city}', [CityController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [CityController::class, 'allCities'])->name('read.all');
    });

    Route::group(['prefix' => 'companies', 'as' => 'companies.'], function () {
        Route::group([], function () {
            Route::put('/{company}', [CompanyController::class, 'update'])->name('update');
            Route::get('/', [CompanyController::class, 'index'])->name('read.index');
            Route::post('/', [CompanyController::class, 'store'])->name('store');
        });
        Route::get('/all/data', [CompanyController::class, 'allCompanies'])->name('read.all');
        Route::get('/{id}/pay-groups', [CompanyController::class, 'allPayGroups'])->name('read.payGroups');
        Route::get('/{id}/sponsors', [CompanyController::class, 'allSponsors'])->name('read.sponsors');
        Route::get('/{company}/employees', [CompanyController::class, 'employees'])->name('read.employees');
        Route::get('/{company}/attendance/employees', [CompanyController::class, 'attendanceEmployees'])->name('read.attendanceEmployees');
    });
    Route::group(['prefix' => 'accounts', 'as' => 'accounts.'], function () {
        Route::group([], function () {
            Route::get('/', [AccountController::class, 'index'])->name('read.index');
            Route::post('/', [AccountController::class, 'store'])->name('store');
            Route::put('/{account}', [AccountController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [AccountController::class, 'allAccount'])->name('read.all');
    });
    Route::group(['prefix' => 'contract-types', 'as' => 'contractTypes.'], function () {
        Route::group([], function () {
            Route::get('/', [ContractTypeController::class, 'index'])->name('read.index');
            Route::post('/', [ContractTypeController::class, 'store'])->name('store');
            Route::put('/{contractType}', [ContractTypeController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [ContractTypeController::class, 'allContractTypes'])->name('read.all');
    });

    Route::group(['prefix' => 'cost-centers', 'as' => 'costCenters.'], function () {
        Route::group([], function () {
            Route::get('/', [CostCenterController::class, 'index'])->name('read.index');
            Route::post('/', [CostCenterController::class, 'store'])->name('store');
            Route::put('/{costCenter}', [CostCenterController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [CostCenterController::class, 'allCostCenters'])->name('read.all');
    });
    Route::group(['prefix' => 'countries', 'as' => 'countries.'], function () {
        Route::group([], function () {
            Route::get('/', [CountryController::class, 'index'])->name('read.index');
            Route::post('/', [CountryController::class, 'store'])->name('store');
            Route::put('/{country}', [CountryController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [CountryController::class, 'allCountries'])->name('read.all');
        Route::get('/{id}/cities', [CountryController::class, 'cities'])->name('read.cities');
    });
    Route::group(['prefix' => 'currencies', 'as' => 'currencies.'], function () {
        Route::group([], function () {
            Route::get('/', [CurrencyController::class, 'index'])->name('read.index');
            Route::post('/', [CurrencyController::class, 'store'])->name('store');
            Route::put('/{currency}', [CurrencyController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [CurrencyController::class, 'allCurrencies'])->name('read.all');
    });
    Route::group(['prefix' => 'departments', 'as' => 'departments.'], function () {
        Route::group([], function () {
            Route::get('/', [DepartmentController::class, 'index'])->name('read.index');
            Route::post('/', [DepartmentController::class, 'store'])->name('store');
            Route::put('/{department}', [DepartmentController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [DepartmentController::class, 'allDepartments'])->name('read.all');
        Route::get('/{id}/sub-departments', [DepartmentController::class, 'allSubDeartments'])->name('read.subDeartments');
    });

    Route::group(['prefix' => 'designations', 'as' => 'designations.'], function () {
        Route::group([], function () {
            Route::get('/', [DesignationController::class, 'index'])->name('read.index');
            Route::post('/', [DesignationController::class, 'store'])->name('store');
            Route::put('/{designation}', [DesignationController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [DesignationController::class, 'allDesignations'])->name('read.all');
    });

    Route::group(['prefix' => 'document-statues', 'as' => 'documentStatues.'], function () {
        Route::group([], function () {
            Route::get('/', [DocumentStatusController::class, 'index'])->name('read.index');
            Route::post('/', [DocumentStatusController::class, 'store'])->name('store');
            Route::put('/{documentStatus}', [DocumentStatusController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [DocumentStatusController::class, 'allDocumentStatus'])->name('read.all');
    });
    Route::group(['prefix' => 'document-types', 'as' => 'documentTypes.'], function () {
        Route::group([], function () {
            Route::get('/', [DocumentTypeController::class, 'index'])->name('read.index');
            Route::post('/', [DocumentTypeController::class, 'store'])->name('store');
            Route::put('/{documentType}', [DocumentTypeController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [DocumentTypeController::class, 'allDocumentType'])->name('read.all');
    });

    Route::group(['prefix' => 'shifts', 'as' => 'shifts.'], function () {
        Route::group([], function () {
            Route::get('/', [ShiftController::class, 'index'])->name('read.index');
            Route::post('/', [ShiftController::class, 'store'])->name('store');
            Route::put('/{shift}', [ShiftController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [ShiftController::class, 'allShifts'])->name('read.all');
    });

    Route::group(['prefix' => 'employee-categories', 'as' => 'employeeCategories.'], function () {
        Route::group([], function () {
            Route::get('/', [EmployeeCategoryController::class, 'index'])->name('read.index');
            Route::post('/', [EmployeeCategoryController::class, 'store'])->name('store');
            Route::put('/{employeeCategory}', [EmployeeCategoryController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [EmployeeCategoryController::class, 'allCategories'])->name('read.all');
    });

    Route::group(['prefix' => 'employee-statues', 'as' => 'employeeStatues.'], function () {
        Route::group([], function () {
            Route::get('/', [EmployeeStatusController::class, 'index'])->name('read.index');
            Route::post('/', [EmployeeStatusController::class, 'store'])->name('store');
            Route::put('/{employeeStatus}', [EmployeeStatusController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [EmployeeStatusController::class, 'allStatues'])->name('read.all');
    });
    Route::group(['prefix' => 'employee-types', 'as' => 'employeeTypes.'], function () {
        Route::group([], function () {
            Route::get('/', [EmployeeTypeController::class, 'index'])->name('read.index');
            Route::post('/', [EmployeeTypeController::class, 'store'])->name('store');
            Route::put('/{employeeType}', [EmployeeTypeController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [EmployeeTypeController::class, 'allTypes'])->name('read.all');
    });

    Route::group(['prefix' => 'gratuity-rules', 'as' => 'gratuityRules.'], function () {
        Route::get('/', [GratuityRuleController::class, 'index'])->name('read.index');
        Route::post('/', [GratuityRuleController::class, 'store'])->name('store');
        Route::put('/{gratuityRule}', [GratuityRuleController::class, 'update'])->name('update');
        Route::get('/all/data', [GratuityRuleController::class, 'allRules'])->name('read.gratuityRules');
    });

    Route::group(['prefix' => 'leave-per-annums', 'as' => 'leavePerAnnums.'], function () {
        Route::group([], function () {
            Route::get('/', [LeavePerAnnumController::class, 'index'])->name('read.index');
            Route::post('/', [LeavePerAnnumController::class, 'store'])->name('store');
            Route::put('/{leavePerAnnum}', [LeavePerAnnumController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [LeavePerAnnumController::class, 'allLeavePerAnnums'])->name('read.all');
    });
    Route::group(['prefix' => 'leave-types', 'as' => 'leaveTypes.'], function () {
        Route::group([], function () {
            Route::get('/', [LeaveTypeController::class, 'index'])->name('read.index');
            Route::post('/', [LeaveTypeController::class, 'store'])->name('store');
            Route::put('/{type}', [LeaveTypeController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [LeaveTypeController::class, 'allTypes'])->name('read');
        Route::get('/employees', [LeaveTypeController::class, 'employees'])->name('read.employees');
    });


    Route::group(['prefix' => 'position-categories', 'as' => 'positionCategories.'], function () {
        Route::group([], function () {
            Route::get('/', [PositionCategoryController::class, 'index'])->name('read.index');
            Route::post('/', [PositionCategoryController::class, 'store'])->name('store');
            Route::put('/{positionCategory}', [PositionCategoryController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [PositionCategoryController::class, 'allCategories'])->name('read.all');
    });

    Route::group(['prefix' => 'projects', 'as' => 'projects.'], function () {
        Route::group([], function () {
            Route::get('/', [ProjectController::class, 'index'])->name('read.index');
            Route::post('/', [ProjectController::class, 'store'])->name('store');
            Route::put('/{project}', [ProjectController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [ProjectController::class, 'allProjects'])->name('read.all');
    });

    Route::group(['prefix' => 'relations', 'as' => 'relations.'], function () {
        Route::group([], function () {
            Route::get('/', [RelationController::class, 'index'])->name('read.index');
            Route::post('/', [RelationController::class, 'store'])->name('store');
            Route::put('/{relation}', [RelationController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [RelationController::class, 'allRelations'])->name('read.all');
    });

    Route::group(['prefix' => 'religions', 'as' => 'religions.'], function () {
        Route::get('/', [ReligionController::class, 'index'])->name('read.index');
        Route::post('/', [ReligionController::class, 'store'])->name('store');
        Route::put('/{religion}', [ReligionController::class, 'update'])->name('update');
        Route::get('/all/data', [ReligionController::class, 'allReligions'])->name('read.all');
    });

    Route::group(['prefix' => 'routes', 'as' => 'routes.'], function () {
        Route::group([], function () {
            Route::get('/', [RouteController::class, 'index'])->name('read.index');
            Route::post('/', [RouteController::class, 'store'])->name('store');
            Route::put('/{route}', [RouteController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [RouteController::class, 'allRoutes'])->name('read.all');
    });

    Route::group(['prefix' => 'sponsors', 'as' => 'sponsors.'], function () {
        Route::group([], function () {
            Route::get('/', [SponsorController::class, 'index'])->name('read.index');
            Route::post('/', [SponsorController::class, 'store'])->name('store');
            Route::put('/{sponsor}', [SponsorController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [SponsorController::class, 'allSponsors'])->name('read.all');
    });


    Route::group(['prefix' => 'sub-departments', 'as' => 'subDepartments.'], function () {
        Route::group([], function () {
            Route::get('/', [SubDepartmentController::class, 'index'])->name('read.index');
            Route::post('/', [SubDepartmentController::class, 'store'])->name('store');
            Route::put('/{subDepartment}', [SubDepartmentController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [SubDepartmentController::class, 'allSubDepartments'])->name('read.all');
    });


    Route::group(['prefix' => 'ticket-entitlements', 'as' => 'ticketEntitlements.'], function () {
        Route::group([], function () {
            Route::get('/', [TicketEntitlementController::class, 'index'])->name('read.index');
            Route::post('/', [TicketEntitlementController::class, 'store'])->name('store');
            Route::put('/{ticketEntitlement}', [TicketEntitlementController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [TicketEntitlementController::class, 'allTicketEntitlements'])->name('read.all');
    });

    Route::group(['prefix' => 'visa-statues', 'as' => 'visaStatues.'], function () {
        Route::group([], function () {
            Route::get('/', [VisaStatusController::class, 'index'])->name('read.index');
            Route::post('/', [VisaStatusController::class, 'store'])->name('store');
            Route::put('/{visaStatus}', [VisaStatusController::class, 'update'])->name('update');
        });
        Route::get('/all/data', [VisaStatusController::class, 'allStatues'])->name('read.all');
    });

    Route::group(['prefix' => 'roles', 'as' => 'roles.'], function () {
        Route::group([], function () {
            Route::get('/', [RoleController::class, 'index'])->name('read.index');
            Route::post('/', [RoleController::class, 'store'])->name('store');
            Route::put('/{role}', [RoleController::class, 'update'])->name('update');
            Route::get('/all/data', [RoleController::class, 'allRoles'])->name('read');
        });
    });

    Route::group(['prefix' => 'permissions', 'as' => 'permissions.'], function () {
        Route::group([], function () {
            Route::get('/', [PermissionController::class, 'index'])->name('read.index');
            Route::post('/', [PermissionController::class, 'store'])->name('store');
            Route::put('/{permission}', [PermissionController::class, 'update'])->name('update');
            Route::get('/all/data', [PermissionController::class, 'allPermissions'])->name('read');
        });
    });

    Route::group(['prefix' => 'event-types', 'as' => 'eventTypes.'], function () {
        Route::get('/', [EventTypeController::class, 'index'])->name('read.index');
        Route::post('/', [EventTypeController::class, 'store'])->name('store');
        Route::put('/{eventType}', [EventTypeController::class, 'update'])->name('update');
        Route::get('/all/data', [EventTypeController::class, 'allEventTypes'])->name('read');
    });

    Route::group(['prefix' => 'event-details', 'as' => 'eventDetails.'], function () {
        Route::get('/', [EventDetailController::class, 'index'])->name('read.index');
        Route::post('/', [EventDetailController::class, 'store'])->name('store');
        Route::put('/{eventDetail}', [EventDetailController::class, 'update'])->name('update');
        Route::get('/all/data', [EventDetailController::class, 'allEvents'])->name('read');
    });

    Route::group(['prefix' => 'notifications', 'as' => 'notifications.'], function () {
        Route::get('/', [NotificationController::class, 'index'])->name('read.index');
        Route::get('/all/data', [NotificationController::class, 'allNotifications'])->name('read');
    });
});
