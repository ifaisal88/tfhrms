<?php

namespace App\Http\Controllers;

use App\Models\AssetBrand;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Setting\AssetBrandRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class AssetBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.assets.assetsBrands');
    }

    public function allBrands()
    {
        $brands = AssetBrand::all();
        return response()->json($brands);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetBrandRequest $request)
    {
        try{
            AssetBrand::create($request->all());
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
            // throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetBrand  $assetBrand
     * @return \Illuminate\Http\Response
     */
    public function show(AssetBrand $assetBrand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssetBrand  $assetBrand
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetBrand $assetBrand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetBrand  $assetBrand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssetBrand $assetBrand)
    {
        try{
            $assetBrand->update($request->all());
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
            // throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetBrand  $assetBrand
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetBrand $assetBrand)
    {
        //
    }
}
