<?php

namespace App\Http\Controllers;

use App\Models\SubDepartment;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Requests\Setting\SubDepartmentRequest;

class SubDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.subDepartments.subDepartments');
    }

    public function allSubDepartments()
    {
        $subDepartments = SubDepartment::with('department')->get();
        return response()->json($subDepartments);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubDepartmentRequest $request)
    {
        try {
            SubDepartment::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\subDepartment  $subDepartment
     * @return \Illuminate\Http\Response
     */
    public function show(subDepartment $subDepartment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\subDepartment  $subDepartment
     * @return \Illuminate\Http\Response
     */
    public function edit(subDepartment $subDepartment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\subDepartment  $subDepartment
     * @return \Illuminate\Http\Response
     */
    public function update(SubDepartmentRequest $request, subDepartment $subDepartment)
    {
        try {
            $subDepartment->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\subDepartment  $subDepartment
     * @return \Illuminate\Http\Response
     */
    public function destroy(subDepartment $subDepartment)
    {
        //
    }
}
