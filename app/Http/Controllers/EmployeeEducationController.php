<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeEducation;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\EmployeeEducationRequest;
use App\Models\Qualification;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class EmployeeEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeEducation  $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function show(employeeEducation $employeeEducation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeEducation  $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeEducation $employeeEducation)
    {
        //
    }

    public function download(EmployeeEducation $employeeEducation)
    {
        $file = '/' . $employeeEducation->employee->employee_id . '/' . $employeeEducation->degree_image;
        return Storage::disk('media_documents')->download($file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeEducation  $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeEducationRequest $request, employeeEducation $employeeEducation)
    {
        $request->merge([
            'attested' => $request->attested == 'true' ? true : false,
            'hardcopy' => $request->hardcopy == 'true' ? true : false
        ]);
        try {
            if (!is_null($request->file('degree'))) {
                $file = $request->file('degree');
                $ext = $file->extension();
                $qualification = Qualification::find($request->qualification_id);
                $name = $qualification->name . '.' . $ext;
                $content = File::get($file->getRealPath());
                $filePath = '/' . $employeeEducation->employee->employee_id . '/' .$name;
                if (Storage::disk('media_documents')->exists($filePath)) {
                    Storage::disk('media_documents')->delete($filePath);
                }
                if (Storage::disk('media_documents')->put($filePath, $content)) {
                    $request->merge(['degree_image' => $name]);
                    $employeeEducation->update($request->all());
                }
            }else{
                $employeeEducation->update($request->all());
            }
            return response()->json(['success' => 'Employee education updated.']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeEducation  $employeeEducation
     * @return \Illuminate\Http\Response
     */
    public function destroy(employeeEducation $employeeEducation)
    {
        //
    }
}
