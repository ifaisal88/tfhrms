<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsuranceCompany;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Requests\Setting\InsuranceCompanyRequest;

class InsuranceCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.insurance.insuranceCompanies');
    }

    public function allInsuranceComapnies()
    {
        $companies = InsuranceCompany::all();
        return response()->json($companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsuranceCompanyRequest $request)
    {
        try{
            InsuranceCompany::create($request->all());
            return response()->json(['success' => 'Record added successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\insuranceCompany  $insuranceCompany
     * @return \Illuminate\Http\Response
     */
    public function show(insuranceCompany $insuranceCompany)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\insuranceCompany  $insuranceCompany
     * @return \Illuminate\Http\Response
     */
    public function edit(insuranceCompany $insuranceCompany)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\insuranceCompany  $insuranceCompany
     * @return \Illuminate\Http\Response
     */
    public function update(InsuranceCompanyRequest $request, insuranceCompany $insuranceCompany)
    {
        try{
            $insuranceCompany->update($request->all());
            return response()->json(['success' => 'Record updated successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\insuranceCompany  $insuranceCompany
     * @return \Illuminate\Http\Response
     */
    public function destroy(insuranceCompany $insuranceCompany)
    {
        //
    }
}
