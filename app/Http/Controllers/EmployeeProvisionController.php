<?php

namespace App\Http\Controllers;

use App\Models\EmployeeProvision;
use Illuminate\Http\Request;

class EmployeeProvisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employeeProvision  $employeeProvision
     * @return \Illuminate\Http\Response
     */
    public function show(employeeProvision $employeeProvision)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeProvision  $employeeProvision
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeProvision $employeeProvision)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeProvision  $employeeProvision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employeeProvision $employeeProvision)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeProvision  $employeeProvision
     * @return \Illuminate\Http\Response
     */
    public function destroy(employeeProvision $employeeProvision)
    {
        //
    }
}
