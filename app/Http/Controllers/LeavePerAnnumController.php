<?php

namespace App\Http\Controllers;

use App\Models\LeavePerAnnum;
use App\Http\Requests\Setting\LeavePerAnnumRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class LeavePerAnnumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.leavePerAnnums.leavePerAnnums');
    }

    public function allLeavePerAnnums()
    {
        $leaves = LeavePerAnnum::all();
        return response()->json($leaves);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeavePerAnnumRequest $request)
    {
        try {
            LeavePerAnnum::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\leavePerAnnum  $leavePerAnnum
     * @return \Illuminate\Http\Response
     */
    public function show(leavePerAnnum $leavePerAnnum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\leavePerAnnum  $leavePerAnnum
     * @return \Illuminate\Http\Response
     */
    public function edit(leavePerAnnum $leavePerAnnum)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\leavePerAnnum  $leavePerAnnum
     * @return \Illuminate\Http\Response
     */
    public function update(LeavePerAnnumRequest $request, leavePerAnnum $leavePerAnnum)
    {
        try {
            $leavePerAnnum->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\leavePerAnnum  $leavePerAnnum
     * @return \Illuminate\Http\Response
     */
    public function destroy(leavePerAnnum $leavePerAnnum)
    {
        //
    }
}
