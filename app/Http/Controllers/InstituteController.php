<?php

namespace App\Http\Controllers;

use App\Models\Institute;
use Illuminate\Http\Request;
use App\Http\Requests\Setting\InstituteRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class InstituteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.institutes.institutes');
    }

    public function allInstitutes()
    {
        $institutes = Institute::with('country')->get();
        return response()->json($institutes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InstituteRequest $request)
    {
        try {
            Institute::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        return response()->json(['success' => 'Record Added!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function show(institute $institute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function edit(institute $institute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function update(InstituteRequest $request, Institute $institute)
    {
        try {
            $institute->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        return response()->json(['success' => 'Record Updated!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\institute  $institute
     * @return \Illuminate\Http\Response
     */
    public function destroy(institute $institute)
    {
        //
    }
}
