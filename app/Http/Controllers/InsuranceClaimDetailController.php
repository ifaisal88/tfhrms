<?php

namespace App\Http\Controllers;

use App\Models\InsuranceClaimDetail;
use Illuminate\Http\Request;

class InsuranceClaimDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\InsuranceClaimDetail  $insuranceClaimDetail
     * @return \Illuminate\Http\Response
     */
    public function show(InsuranceClaimDetail $insuranceClaimDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\InsuranceClaimDetail  $insuranceClaimDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(InsuranceClaimDetail $insuranceClaimDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\InsuranceClaimDetail  $insuranceClaimDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsuranceClaimDetail $insuranceClaimDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\InsuranceClaimDetail  $insuranceClaimDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsuranceClaimDetail $insuranceClaimDetail)
    {
        //
    }
}
