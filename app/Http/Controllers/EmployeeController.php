<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use App\Models\User;
use App\Models\EmployeeType;
use App\Models\VisaStatus;
use App\Models\EmployeeCategory;
use App\Models\Company;
use App\Models\Sponsor;
use App\Models\PayGroup;
use App\Models\Country;
use App\Models\City;
use App\Models\Relation;
use App\Models\Religion;
use App\Models\EmployeeDependant;
use App\Models\Qualification;
use App\Models\Institute;
use App\Models\Bank;
use App\Models\DocumentType;
use App\Models\DocumentStatus;
use App\Models\AssetType;
use App\Models\AssetCondition;
use App\Models\AssetBrand;
use App\Models\Currency;
use App\Models\InsuranceCategory;
use App\Models\InsuranceCompany;
use App\Models\Route;
use App\Models\PositionCategory;
use App\Models\Designation;
use App\Models\Department;
use App\Models\SubDepartment;
use App\Models\CostCenter;
use App\Models\Project;
use App\Models\TicketEntitlement;
use App\Models\EmployeeStatus;
use App\Models\ContractType;
use App\Models\Shift;
use App\Models\LeavePerAnnum;
// Reources
use App\Http\Resources\Employee\UserResource;
use App\Http\Resources\Employee\UserOfficalTabsResource;
use App\Http\Resources\Employee\EmployeeProfileResource;

use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;

// Form Requests
use Illuminate\Http\Request;
use App\Http\Requests\EditUserProfile;
use App\Http\Requests\AddEmployeeRequest;
use App\Http\Requests\EmployeePersonalInfoRequest;
use App\Http\Requests\EmployeeContactRequest;
use App\Http\Requests\EmployeeDependantRequest;
use App\Http\Requests\EmergencyContactRequest;
use App\Http\Requests\EmployeeEducationRequest;
use App\Http\Requests\EmployeeOfficialDetailsRequest;
use App\Http\Requests\EmployeeRuleRequest;
use App\Http\Requests\EmployeePayElementRequest;
use App\Http\Requests\EmployeeBankInfoRequest;
use App\Http\Requests\EmployeeInsuranceRequest;
use App\Http\Requests\EmployeeDocumentRequest;
use App\Http\Requests\EmployeeAssetRequest;
use App\Http\Requests\EmployeeDependentBenefitRequest;
use App\Http\Resources\Employee\LineManagerMainResource;
use App\Models\EmployeeAttendance;
use Illuminate\Support\Str;

use App\Exports\SalaryExport;
use App\Http\Resources\Employee\EmployeesDataTableMainResource;
use App\Models\AttendanceStatus;
use App\Models\EmployeeAllowance;
use App\Models\EmployeeAssetImage;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = User::get()->all();
        return view('employees.list', ['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function allDetails($id)
    {
        $employee = User::find($id);
        return response()->json($employee);
    }

    public function unassigned(Request $request)
    {
        $employees = User::where('line_manager_id', null)->where('employee_id','!=', $request->line_manager)->get();
        return new LineManagerMainResource($employees);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddEmployeeRequest $request)
    {
        // $employee = User::create([
        //     'employee_id' => Str::random(10),
        //     'personal_id' => $request->personal_id,
        //     'emirates_id' => $request->emirates_id,
        //     'first_name' => $request->first_name,
        //     'middle_name' => $request->middle_name,
        //     'last_name' => $request->last_name,
        //     'full_name' => $request->first_name . ' ' . $request->middle_name . ' ' . $request->last_name,
        //     'email' => $request->email,
        //     'password' => Hash::make('password'),
        //     'home_phone_number' => $request->home_phone_number,
        //     'profile_image' => 'default_image.jpg',
        //     'status' => $request->status
        // ]);
        $request->merge([
            'password' => Hash::make('password'),
            'labor_card_number' => Str::random(10),
            'person_code' => Str::random(10)
        ]);
        try {
            User::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(user $employee)
    {
        // return [$employee];
        $employee = User::with([
            'employeeType',
            'visaStatus',
            'employeeCategory',
            'contactPeoples.relationName',
            'linemanager',
            'dependants.relation',
            'educations.country',
            'educations.qualification',
            'employeeBankDetail'
        ])->find($employee->employee_id);
        $contactPersons = $employee->contactPeoples;
        $educations = $employee->educations;
        $employeeDependants = $employee->dependants;
        $employeeEducations = $employee->education;
        $employeeDocuments = $employee->documents;
        $employeeDependantsBenefits = $employee->dependantBenefits;
        $employeeAssets = $employee->assets;
        $bankDetails = $employee->bankDetails;
        $employee_types = EmployeeType::select(['id', 'name'])->get();
        $employee_categories = EmployeeCategory::select(['id', 'name'])->get();
        $visa_statuses = VisaStatus::select(['id', 'name'])->get();
        $line_managers = User::select(['employee_id', 'full_name'])->get();
        $companies = Company::select(['id', 'name'])->get();
        $sponsors = Sponsor::select(['id', 'name'])->get();
        $pay_groups = PayGroup::select(['id', 'name'])->get();
        $nationalities = Country::select(['id', 'name'])->get();
        $relations = Relation::select(['id', 'name'])->get();
        $religions = Religion::select(['id', 'name'])->get();
        $document_statuses = DocumentStatus::select(['id', 'name'])->get();
        $document_types = DocumentType::select(['id', 'name'])->get();
        $asset_types = AssetType::select(['id', 'name'])->get();
        $asset_conditions = AssetCondition::select(['id', 'name'])->get();
        $asset_brands = AssetBrand::select(['id', 'name'])->get();
        $banks = Bank::select(['id', 'name'])->get();
        $qualifications = Qualification::select(['id', 'name'])->get();
        $institutes = Institute::select(['id', 'name'])->get();
        $currencies = Currency::select(['id', 'name', 'symbol'])->get();
        $employee_dependent_names = EmployeeDependant::select(['id', 'name'])->where('user_employee_id', $employee->employee_id)->get();
        $insurance_companies = InsuranceCompany::select(['id', 'name'])->get();
        $insurance_categories = InsuranceCategory::select(['id', 'name'])->get();
        $ticket_entitlements = TicketEntitlement::select(['id', 'name'])->get();
        $routes = Route::select(['id', 'name'])->get();
        $positionCategories = PositionCategory::select(['id', 'name'])->get();
        $designations = Designation::select(['id', 'name'])->get();
        $departments = Department::select(['id', 'name'])->get();
        $subDepartments = SubDepartment::select(['id', 'name'])->get();
        $cities = City::select(['id', 'name', 'country_id'])->get();
        $costCenters = CostCenter::select(['id', 'name'])->get();
        $projects = Project::select(['id', 'name'])->get();
        $employeeStatuses = EmployeeStatus::select(['id', 'name'])->get();
        $contractTypes = ContractType::select(['id', 'name'])->get();
        $shifts = Shift::select(['id', 'name'])->get();
        $leaveRules = LeavePerAnnum::select(['id', 'name'])->get();
        $maritialStatus = config('basic.maritial_status');
        $bloodGroup = config('basic.blood_group');
        $employeeGender = config('basic.employee_gender');
        $probationPeriod = config('basic.probation_period');

        return view('employees.profile', [
            'contractTypes'             => $contractTypes,
            'shifts'                    => $shifts,
            'leaveRules'                => $leaveRules,
            'countries'                 => $nationalities,
            'employee'                  => $employee,
            'employee_types'            => $employee_types,
            'visa_statuses'             => $visa_statuses,
            'employee_categories'       => $employee_categories,
            'line_managers'             => $line_managers,
            'companies'                 => $companies,
            'sponsors'                  => $sponsors,
            'pay_groups'                => $pay_groups,
            'nationalities'             => $nationalities,
            'relations'                 => $relations,
            'religions'                 => $religions,
            'employeeDependants'        => $employeeDependants,
            'qualifications'            => $qualifications,
            'institutes'                => $institutes,
            'contactPersons'            => $contactPersons,
            'educations'                => $educations,
            'bankDetails'               => $bankDetails,
            'banks'                     => $banks,
            'document_statuses'         => $document_statuses,
            'document_types'            => $document_types,
            'employeeDocuments'         => $employeeDocuments,
            'asset_types'               => $asset_types,
            'asset_conditions'          => $asset_conditions,
            'asset_brands'              => $asset_brands,
            'currencies'                => $currencies,
            'employeeAssets'            => $employeeAssets,
            'employee_dependent_names'  => $employee_dependent_names,
            'insurance_companies'       => $insurance_companies,
            'insurance_categories'      => $insurance_categories,
            'ticket_entitlements'       => $ticket_entitlements,
            'routes'                    => $routes,
            'employeeDependantsBenefits' => $employeeDependantsBenefits,
            'maritialStatus'            => $maritialStatus,
            'bloodGroup'                => $bloodGroup,
            'employeeGender'            => $employeeGender,
            'probationPeriod'           => $probationPeriod,
            'positionCategories'        => $positionCategories,
            'designations'              => $designations,
            'departments'               => $departments,
            'subDepartments'            => $subDepartments,
            'cities'                    => $cities,
            'costCenters'               => $costCenters,
            'projects'                  => $projects,
            'employeeStatuses'          => $employeeStatuses
        ]);
    }

    public function basicInfo($id)
    {
        // $employee = User::with([
        //     'linemanager',
        //     'company',
        //     'sponsor',
        //     'payGroup',
        //     'nationality',
        //     'religion',
        //     'country',
        //     'designation',
        //     'department',
        //     'subDepartment',
        //     'positionCategory',
        //     'dependants.relation',
        //     'dependants.country',
        //     'visaStatus',
        //     'employeeType',
        //     'visaStatus',
        //     'employeeCategory',
        //     'contactPeoples.relationName',
        //     'educations.country',
        //     'educations.qualification',
        //     'educations.institute',
        //     'employeeBankDetail',
        //     'officalDetails'
        // ])->find($id);
        $employee = User::find($id);
        // return response()->json($employee->dependantBenefits);
        return new UserResource($employee);
    }

    public function officalInfo($id)
    {
        $employee = User::with(['assets.assetBrand', 'assets.assetCondition', 'dependantBenefits'])->find($id);
        // return response()->json($employee->dependantBenefits);
        // return response()->json($employee->bankDetails);
        // return [$employee->documents];
        // return response()->json($employee);
        return new UserOfficalTabsResource($employee);
    }

    public function profile(User $user)
    {
        return new EmployeeProfileResource($user);
    }

    public function allEmployees()
    {
        $users = User::all();
        return new EmployeesDataTableMainResource($users);
        // return response()->json($users);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(user $employee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserProfile $request, user $employee)
    {
        try {
            $employee->update($request->all());
            return response()->json(['success' => 'Employee Updated']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $employee)
    {
        try {
            $employee->delete();
            return response()->json(['success' => 'Employee Deleted']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    public function storeContactPerson($id, EmergencyContactRequest $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $contactPerson = \App\Models\ContactPeople::create($request->all());
        return response()->json(['success' => 'Employee Emergency Contact Added']);
    }

    public function storeDependants($id, EmployeeDependantRequest $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $contactPerson = \App\Models\EmployeeDependant::create($request->all());
        return redirect()->back();
    }

    public function storeEducation($id, EmployeeEducationRequest $request)
    {

        $request->merge([
            'attested' => $request->attested == 'true' ? true : false,
            'hardcopy' => $request->hardcopy == 'true' ? true : false
        ]);
        $createRecod = false;
        if (!is_null($request->file('degree'))) {
            $file = $request->file('degree');
            $ext = $file->extension();
            $qualification = Qualification::find($request->qualification_id);
            $name = $qualification->name . '.' . $ext;
            $content = File::get($file->getRealPath());
            $path = $id . "/" . $name;
            if (Storage::disk('media_documents')->put($path, $content)) {
                $request->merge(['degree_image' => $name]);
                $createRecod = true;
            }
        } else {
            $createRecod = true;
        }
        if ($createRecod) {
            $employeeEducation = \App\Models\EmployeeEducation::create($request->all());
        }

        return response()->json(['success' => 'Employee Education Added']);
    }

    public function storeDocuments($id, EmployeeDocumentRequest $request)
    {
        if (!is_null($request->file('document'))) {
            $file = $request->file('document');
            $ext = $file->extension();
            $documentType = DocumentType::find($request->document_type_id);
            $name = $documentType->name . '.' . $ext;
            $content = File::get($file->getRealPath());
            $path = $id . "/" . $name;
            if (Storage::disk('media_documents')->put($path, $content)) {
                $request->merge(['document_path' => $name]);
            }
        }
        $employeeDocument = \App\Models\EmployeeDocument::create($request->all());
        return response()->json(['success' => 'Employee Document Added']);
    }

    public function storeAccountDetails($id, EmployeeBankInfoRequest $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $employeeEducation = \App\Models\EmployeeBankDetail::create($request->all());
        return response()->json(['success' => 'Employee Document Added']);
    }

    public function storeAssets($id, EmployeeAssetRequest $request)
    {
        DB::beginTransaction();
        try {
            $employeeAsset = \App\Models\EmployeeAsset::create($request->all());
            $images = [];
            if (!is_null($request->assign_images)) {
                foreach ($request->assign_images as $key => $image) {
                    $content = file_get_contents($image['path']);
                    $ext = explode(".", $image['name']);
                    $name = str_replace(" ", "-", strtolower($employeeAsset->assetItem->name . ($key + 1) . "-assign." . $ext[1]));
                    $path = $employeeAsset->id . '/' . $name;
                    if (!Storage::disk('media_assets')->has($employeeAsset->id . '/' . $name)) {
                        Storage::disk('media_assets')->put($path, $content);
                        $images[] = [
                            'employee_asset_id' => $employeeAsset->id,
                            'image' => $name,
                            'type' => 'assigning'
                        ];
                    }
                }
            }
            if (!is_null($request->return_images)) {
                foreach ($request->return_images as $key => $image) {
                    $content = file_get_contents($image['path']);
                    $ext = explode(".", $image['name']);
                    $name = str_replace(" ", "-", strtolower($employeeAsset->assetItem->name . ($key + 1) . "-return." . $ext[1]));
                    $path = $employeeAsset->id . '/' . $name;
                    if (!Storage::disk('media_assets')->has($employeeAsset->id . '/' . $name)) {
                        Storage::disk('media_assets')->put($path, $content);
                        $images[] = [
                            'employee_asset_id' => $employeeAsset->id,
                            'image' => $name,
                            'type' => 'return'
                        ];
                    }
                }
            }
            foreach ($images as $imageStore) {
                EmployeeAssetImage::create($imageStore);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        

        return response()->json(['success' => 'Employee Asset Added']);
    }

    public function storeDependantBenefits($id, EmployeeDependentBenefitRequest $request)
    {
        $request->merge(['user_employee_id' => $id]);
        $employeeDependantBenefits = \App\Models\EmployeeDependantBenefit::create($request->all());
        return redirect()->back();
    }

    public function updateEmployeeMainInfo(Request $request, $id)
    {
        $user = User::find($id);
        $update = $user->update($request->all());
        return response()->json(['success' => 'Employee Main Infromation Updated']);
    }

    public function updateEmployeePersonalDetails(EmployeePersonalInfoRequest $request, User $id)
    {
        try {
            $id->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    public function updateEmployeeContactDetails(EmployeeContactRequest $request, User $user)
    {
        try {
            $user->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    public function updateEmployeeOfficialDetails(EmployeeOfficialDetailsRequest $request, $id)
    {
        $user = User::find($id);
        $date = new Carbon($request->date_of_joining);
        $endDate = $date->addMonths($request->probation_period);
        $request->merge(['probation_end_date' => $endDate]);
        $update = $user->update($request->all());
        return response()->json(['success' => 'User Offical Infromation Updated']);
    }

    public function updateEmployeePayElements(EmployeePayElementRequest $request, User $user)
    {
        try {
            DB::beginTransaction();
            $user->update($request->all());
            foreach ($request->employee_allowances as $allowance) {
                EmployeeAllowance::updateOrCreate([
                    'user_employee_id' => $user->employee_id,
                    'allowance_id' => $allowance['id'],
                ], [
                    'amount' => $allowance['amount']
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    public function updateInsuranceDetails(EmployeeInsuranceRequest $request, User $user)
    {
        try {
            $user->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    public function updateEmployeeRules(EmployeeRuleRequest $request, $id)
    {
        $user = User::find($id);
        $update = $user->update($request->all());
        return response()->json(['success' => 'User Rules Added']);
    }

    public function updateProfileImage(Request $request, $id)
    {
        $image = $request->file('profile_image');
        $name = 'profile.jpg';
        $path = $id . '/' . $name;
        if (Storage::disk('media_images')->exists($path)) {
            Storage::disk('media_images')->delete($path);
        }
        $img = \Image::make($image);
        $img->resize(200, 200);
        $img->encode('png');

        $width = '200'; //$img->width();
        $height = '200'; //$img->height();
        $mask = \Image::canvas($width, $height);

        $mask->circle($width, $width / 2, $height / 2, function ($draw) {
            $draw->background('#fff');
        });

        $img->mask($mask, false);

        $img->stream();

        Storage::disk('media_images')->put($path, $img);
        return redirect()->back();
    }

    public function dependents($id)
    {
        $dependents = EmployeeDependant::where('user_employee_id', $id)->get();
        return response()->json($dependents);
    }

    public function unsetLinemanager(Request $request)
    {
        try {
            $employees = $request->employees;
            User::unsetLinemanager($employees);
            return response()->json(['success' => 'Employees Updated']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    public function leaveEmployees(Request $request)
    {
        $startDate  = new Carbon($request->startDate);
        $endDate  = new Carbon($request->endDate);

        $leavesData = [];

        $employees = User::with(['employeeLeaves' => function ($query) use ($startDate, $endDate) {
            $query->where(function ($q1) use ($startDate, $endDate) {
                $q1->whereDate('start_date', '>=', $startDate)->whereDate('end_date', '<=', $endDate);
            })->orWhere(function ($q2) use ($startDate, $endDate) {
                $q2->whereDate('start_date', '<', $startDate)->whereDate('end_date', '>', $startDate);
            })->orWhere(function ($q3) use ($startDate, $endDate) {
                $q3->whereDate('start_date', '>', $startDate)->whereDate('start_date', '<', $endDate)->whereDate('end_date', '>', $endDate);
            })->orWhere(function ($q4) use ($startDate, $endDate) {
                $q4->whereDate('start_date', '<', $startDate)->whereDate('end_date', '>', $startDate)->whereDate('end_date', '<', $endDate);
            });
        }, 'employeeLeaves.leaveType', 'employeeLeaves.leaveStatus'])->whereHas('employeeLeaves', function ($query) use ($startDate, $endDate) {
            $query->where(function ($q1) use ($startDate, $endDate) {
                $q1->whereDate('start_date', '>=', $startDate)->whereDate('end_date', '<=', $endDate);
            })->orWhere(function ($q2) use ($startDate, $endDate) {
                $q2->whereDate('start_date', '<', $startDate)->whereDate('end_date', '>', $startDate);
            })->orWhere(function ($q3) use ($startDate, $endDate) {
                $q3->whereDate('start_date', '>', $startDate)->whereDate('start_date', '<', $endDate)->whereDate('end_date', '>', $endDate);
            })->orWhere(function ($q4) use ($startDate, $endDate) {
                $q4->whereDate('start_date', '<', $startDate)->whereDate('end_date', '>', $startDate)->whereDate('end_date', '<', $endDate);
            });
        })->get();

        foreach ($employees as $employee) {
            foreach ($employee->employeeLeaves as $leave) {
                $leavesData[] = [
                    'id' => $employee->employee_id,
                    'employeeId' => $employee->employee_id,
                    'name' => $employee->full_name,
                    'employeeLeaveId' => $leave->id,
                    'leaveType' => [
                        'id' => $leave->leave_type_id,
                        'name' => $leave->leaveType->name,
                    ],
                    'status' => $leave->leaveStatus->status,
                    'pendingBy' => !is_null($leave->leaveStatus->pendingBy) ? $leave->leaveStatus->pendingBy->full_name : "",
                    'approvedBy' => !is_null($leave->leaveStatus->approvedBy) ? $leave->leaveStatus->approvedBy->full_name : "",
                    'submittedDate' => $leave->leaveStatus->submitted_date,
                    'approvedDate' => $leave->leaveStatus->approved_date,
                    'startDate' => $leave->start_date,
                    'endDate' => $leave->end_date,
                    'paidLeave' => $leave->is_paid,
                    'remarks' => $leave->remarks,
                ];
            }
        }
        return response()->json($leavesData);
    }

    public function attendanceSheetDownload(Request $request)
    {

        $employeeData[0] = ['Employee Id', 'Emirates Id', 'Name', 'Company', 'Department', 'Sub Department', 'Date', 'Attendance Status', 'Check In Time', 'Check Out time', 'Remarks'];
        $period = CarbonPeriod::create($request->startDate, $request->endDate);
        $leaveStatus = AttendanceStatus::where('on_leave', true)->first();
        foreach ($period as $date) {

            $employees = User::with(['employeeAttendance' => function ($query) use ($date) {
                $query->where('attendance_date', $date);
            }, 'employeeLeaves' => function ($query) use ($date) {
                $query->whereDate('start_date', '<=', $date)->whereDate('end_date', '>=', $date);
            }])->where('company_id', $request->companyId)->get();

            foreach ($employees as $employee) {
                $status = count($employee->employeeAttendance) > 0 ? $employee->employeeAttendance[0]->attendanceStatus->name : '';
                if (count($employee->employeeLeaves)) {
                    $status = $leaveStatus->name;
                }
                $employeeData[] = [
                    'id' => $employee->employee_id,
                    'emirates id' => $employee->emirates_id,
                    'name' => $employee->full_name,
                    'company' => $employee->company ? $employee->company->name : "",
                    'department' => $employee->department ? $employee->department->name : "",
                    'sub deparment' => $employee->subDepartment ? $employee->subDepartment->name : "",
                    'date' => $date->format('Y-m-d'),
                    'attendance status' => $status,
                    'check in time' => '',
                    'check out time' => '',
                ];
            }
        }
        return Excel::download(new SalaryExport($employeeData), 'Attendance ' . $request->date . '.xlsx');
    }

    public function lineManagerEmployees(User $employee)
    {
        $employees = User::lineManagerEmployees($employee->employee_id)->get();
        return response()->json($employees);
    }

    public function insuranceEmployees()
    {
        $employees = User::where('medical_insurance', true)->get();
        return response()->json($employees);
    }
}
