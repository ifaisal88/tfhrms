<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeAssetImage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image as Image;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeAssetImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeAssetImage  $employeeAssetImage
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeAssetImage $employeeAssetImage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeAssetImage  $employeeAssetImage
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeAssetImage $employeeAssetImage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeAssetImage  $employeeAssetImage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeAssetImage $employeeAssetImage)
    {
        try {
            Storage::disk('media_assets')->delete($employeeAssetImage->asset->id . '/' . $employeeAssetImage->image);
            $path = $employeeAssetImage->asset->id . '/' . $employeeAssetImage->image;
            $content = file_get_contents($request->image_path);
            Storage::disk('media_assets')->put($path, $content);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeAssetImage  $employeeAssetImage
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeAssetImage $employeeAssetImage)
    {
        try {
            if (Storage::disk('media_assets')->delete($employeeAssetImage->asset->id . '/' . $employeeAssetImage->image)) {
                $employeeAssetImage->delete();
            }
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }
}
