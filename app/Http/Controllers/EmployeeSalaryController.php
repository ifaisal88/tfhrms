<?php

namespace App\Http\Controllers;

use PDF;
use File;
use Input;
use App\Exports\AllowancesExport;
use App\Exports\SalaryAllowanceExport;
use App\Exports\SalaryExport;
use App\Http\Resources\PayRoll\SlipResource;
use App\Imports\SalaryAllowanceImport;
use App\Models\Allowance;
use App\Models\Deduction;
use App\Models\EmployeeSalary;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeSalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeSalary  $employeeSalary
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeSalary $employeeSalary)
    {
        // return [$employeeSalary];
        return new SlipResource($employeeSalary);
    }

    /**
     * Return salary slip view.
     *
     * @param  \App\Models\EmployeeSalary  $employeeSalary
     * @return \Illuminate\Http\Response
     */
    public function slip(EmployeeSalary $employeeSalary)
    {
        $salaryData = collect(new SlipResource($employeeSalary));
        return view('payroll.payroll-slip', ['salaryData' => $salaryData]);
    }

    public function slipPdf(EmployeeSalary $employeeSalary)
    {
        // $salaryData = collect(new SlipResource($employeeSalary));
        // // return view('payroll.payroll-slip-pdf', ['salaryData' => $salaryData]);
        // // return [$salaryData];
        // view()->share('salaryData', $salaryData);
        $content = $this->pdfContent();
        return PDF::loadHTML($content)->download('Slip.pdf');
        // $pdf = PDF::loadView('payroll.payroll-slip-pdf', ['salaryData' => $salaryData])->setOptions(['defaultFont' => 'sans-serif']);
        // return $pdf->download('invoice.pdf');
    }

    /**
     * Return salary excel.
     *
     * @param  \App\Models\EmployeeSalary  $employeeSalary
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        $allowances = Allowance::all();
        $deductions = Deduction::all();
        $columns = ['Employee Id', 'Employee Name', 'Start Date', 'End Date', 'Basic Salary'];
        foreach ($allowances as $key => $allowance) {
            $columns[] = $allowance->name . ' Allowance';
        }
        $columns[] = 'Allowances Total';
        $columns[] = 'Gross Salary';
        foreach ($deductions as $key => $deduction) {
            $columns[] = $deduction->name . ' Deductable';
        }
        $columns[] = 'Deductables Total';
        $columns[] = 'Net Salary';
        $exportData[0] = $columns;
        $employees = User::employeesSalary($request->ids, $request->startDate, $request->endDate)->get();
        foreach ($employees as $key => $employee) {
            $data = [
                'Employee Id' => $employee->employee_id,
                'Employee Name' => $employee->full_name,
                'Start Date' => $employee->employeeSalary->start_date,
                'End Date' => $employee->employeeSalary->end_date,
                'Basic Salary' => $employee->employeeSalary->basic_salary,
            ];
            foreach ($allowances as $key => $allowance) {
                $addedAllowance = $employee->employeeSalary->details->where('allowance_id', $allowance->id) ? $employee->employeeSalary->details->where('allowance_id', $allowance->id)->first() : false;
                $data[$allowance->name . ' Allowance'] = $addedAllowance ? $addedAllowance->amount : '0';
            }
            $data['Allowances Total'] = $employee->employeeSalary->allowances_total;
            $data['Gross Salary'] = $employee->employeeSalary->gross_salary;
            foreach ($deductions as $key => $deduction) {
                $addedDeduction = $employee->employeeSalary->details->where('deduction_id', $deduction->id) ? $employee->employeeSalary->details->where('deduction_id', $deduction->id)->first() : false;
                $data[$deduction->name . ' Deductable'] = $addedDeduction ? $addedDeduction->amount : '0';
            }
            $data['Deductables Total'] = $employee->employeeSalary->deductable_total;
            $data['Net Salary'] = $employee->employeeSalary->net_salary;
            $exportData[] = $data;
        }
        // return [$exportData];
        return Excel::download(new SalaryExport($exportData), 'Salary.xlsx');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeSalary  $employeeSalary
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeSalary $employeeSalary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeSalary  $employeeSalary
     * @return \Illuminate\Http\Response
     */
    public function allowanceTemplateDownload(Request $request)
    {
        $data = $allData = [];
        $id = $request->paygroup;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $ids = $request->allowances;
        $employeesIds = $request->employees;

        $employees = User::with('employeeType', 'allowances.allowance')->salaryEmployees($id, $startDate, $endDate)->whereIn('employee_id', $employeesIds)->get();
        $allowances = Allowance::whereIn('id', $ids)->get();
        foreach ($employees as $employee) {
            $data['Name'] = $employee->full_name;
            $data['Employee Id'] = $employee->employee_id;
            $data['Emirates Id'] = $employee->emirates_id;
            $data['Person Code'] = $employee->person_code ? $employee->person_code : '';
            $data['Joining Date'] = $employee->date_of_joining ? $employee->date_of_joining : '';
            $data['Start Date'] = $startDate;
            $data['End Date'] = $endDate;
            foreach ($allowances as $allowance) {
                $allowanceAssigned = !is_null($employee->allowances->where('allowance_id', $allowance->id)) ? $employee->allowances->where('allowance_id', $allowance->id)->first() : 0;
                $data[$allowance->name] = isset($allowanceAssigned->amount) ? $allowanceAssigned->amount : 0;
            }
            $allData[] = $data;
        }
        return Excel::download(new SalaryAllowanceExport($allData, $allowances), 'Allowances.xlsx');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeSalary  $employeeSalary
     * @return \Illuminate\Http\Response
     */
    public function deductableTemplateDownload(Request $request)
    {
        $data = $allData = [];
        $id = $request->paygroup;
        $startDate = $request->startDate;
        $endDate = $request->endDate;
        $ids = $request->deductables;
        $employeesIds = $request->employees;

        $employees = User::salaryEmployees($id, $startDate, $endDate)->whereIn('employee_id', $employeesIds)->get();
        $allowances = Deduction::whereIn('id', $ids)->get();

        foreach ($employees as $employee) {
            $data['Name'] = $employee->full_name;
            $data['Employee Id'] = $employee->employee_id;
            $data['Emirates Id'] = $employee->emirates_id;
            $data['Person Code'] = $employee->person_code ? $employee->person_code : '';
            $data['Joining Date'] = $employee->date_of_joining ? $employee->date_of_joining : '';
            $data['Start Date'] = $startDate;
            $data['End Date'] = $endDate;
            foreach ($allowances as $allowance) {
                $data[$allowance->name] = 0;
            }
            $allData[] = $data;
        }
        return Excel::download(new SalaryAllowanceExport($allData, $allowances), 'Allowances.xlsx');
    }

    public function allowanceTemplateUpload(Request $request)
    {
        $fileName = $request->file('document')->getClientOriginalName() ? $request->file('document')->getClientOriginalName() : null;
        $upload = Storage::disk('uploads')->put($fileName, File::get(Input::file('document')));
        $file = "upload" . DIRECTORY_SEPARATOR . $fileName;
        $data = (new SalaryAllowanceImport)->toArray($file);
        $employees = $data[0];
        $returnData = $allData = $allAllowances = [];
        $ids = explode(',',$request->allowances);

        $allowances = Allowance::whereIn('id', $ids)->get();
        foreach ($employees as $employee) {
            $employeeAllowances = [];
            $returnData = [
                'id' => $employee['employee_id'],
                'name' => $employee['name'],
                'emiratesId' => $employee['emirates_id'],
                'personCode' => $employee['person_code'],
                'dateOfJoining' => $employee['joining_date'],
                'startDate' => $employee['start_date'],
                'endDate' => $employee['end_date'],
            ];
            foreach ($allowances as $allowance) {
                $amount = isset($employee[str_replace(" ", "_", strtolower($allowance->name))]) ? $employee[str_replace(" ", "_", strtolower($allowance->name))] : 0;
                $employeeAllowances['id'] = $allowance->id;
                $employeeAllowances['name'] = $allowance->name;
                $employeeAllowances['amount'] = $amount;
                if($allowance->enable_remarks == 1){
                    $employeeAllowances['remarks'] = $employee[str_replace(" ", "_", strtolower($allowance->name.'_remarks'))];
                }
                $allAllowances[] = $employeeAllowances;
            }
            $returnData['allowances'] = $allAllowances;
            $allData[] = $returnData;
        }
        return response()->json($allData);
    }

    public function deductableTemplateUpload(Request $request)
    {
        $fileName = $request->file('document')->getClientOriginalName() ? $request->file('document')->getClientOriginalName() : null;
        $upload = Storage::disk('uploads')->put($fileName, File::get(Input::file('document')));
        $file = "upload" . DIRECTORY_SEPARATOR . $fileName;
        $data = (new SalaryAllowanceImport)->toArray($file);
        $employees = $data[0];
        $returnData = $allData = $allDeductions = [];
        $ids= $ids = explode(',',$request->deductables);


        $allowances = Deduction::whereIn('id', $ids)->get();
        foreach ($employees as $employee) {
            $employeeAllowances = [];
            $returnData = [
                'id' => $employee['employee_id'],
                'name' => $employee['name'],
                'emiratesId' => $employee['emirates_id'],
                'personCode' => $employee['person_code'],
                'dateOfJoining' => $employee['joining_date'],
                'startDate' => $employee['start_date'],
                'endDate' => $employee['end_date'],
            ];
            foreach ($allowances as $allowance) {
                $amount = isset($employee[str_replace(" ", "_", strtolower($allowance->name))]) ? $employee[str_replace(" ", "_", strtolower($allowance->name))] : 0;
                $employeeAllowances['id'] = $allowance->id;
                $employeeAllowances['name'] = $allowance->name;
                $employeeAllowances['amount'] = $amount;
                if($allowance->enable_remarks == 1){
                    $employeeAllowances['remarks'] = $employee[str_replace(" ", "_", strtolower($allowance->name.'_remarks'))];
                }
                $allDeductions[] = $employeeAllowances;
            }
            $returnData['deductables'] = $allDeductions;
            $allData[] = $returnData;
        }
        return response()->json($allData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeSalary  $employeeSalary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeSalary $employeeSalary)
    {
        try {
            $employeeSalary->basic_salary = $request->basicSalary;
            $employeeSalary->gross_salary = $request->grossSalary;
            $employeeSalary->net_salary = $request->netSalary;
            $employeeSalary->updated_by = Auth::user()->employee_id;
            $employeeSalary->save();
            return response()->json(['success' => 'Slaray Updated!']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeSalary  $employeeSalary
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeSalary $employeeSalary)
    {
        //
    }

    public function pdfContent()
    {
        $content = '<style type="text/css" media="all">
        body {
            padding: 0px 15px;
        }

        .full-width {
            width: 100%;
            float: left;
        }

        .siteInfo {
            width: 85%;
            float: left;
        }

        .invoiceNum {
            width: 14%;
            float: left;
            padding-right: 1%;
        }

        .half-width {
            width: 48.5%;
            float: left;
        }

        .allow_deduct {
            width: 100%;
            text-align: left;
        }

        .allow_deduct th,
        .allow_deduct td {
            padding: 15px 25px;
        }

        .allow_deduct thead th {
            color: #fff;
        }

        .allow_deduct td {
            border-bottom: 1px solid #e4e3e3;
            ;
        }

        .allow thead th {
            background-color: #23af46;
        }

        .deduct thead th {
            background-color: #de4848;
        }

    </style>
        <div class="full-width" style="padding: 10px 5px 15px 5px;border-bottom: 1px solid #e4e3e3;">
        <div class="siteInfo">
            <p>Replace this with Logo</p>
            <p>
                <b style="line-height: 2;">IML GROUP.</b><br>
                8117 Roosevelt St.<br>
                New Rochelle, NY 10801
            </p>
        </div>
        <div class="invoiceNum" style="text-align: right;">
            <h1>Invoice # 22</h1>
            <p>Salary Month: April 2021</p>
        </div>
    </div>
    <div class="full-width" style="padding:10px 0px;">
        <img src="user.png" alt="" style="float:left;">
        <p>Cleaning Supervisor<br>Employee ID: lkX791sbn3</p>
    </div>
    <div class="full-width" style="padding-bottom: 20px;">
        <div class="half-width" style="padding-right:1.5%;">
            <table class="allow_deduct allow" cellspacing=0>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Allowances</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Basic Salary</td>
                        <td>700</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>House</td>
                        <td>800</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Insentive</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Insentive</td>
                        <td>200</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Total Earnings</th>
                        <th>3300</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="half-width" style="padding-left:1.5%;">
            <table class="allow_deduct deduct" cellspacing=0>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Allowances</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Basic Salary</td>
                        <td>700</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>House</td>
                        <td>800</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Insentive</td>
                        <td>200</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Insentive</td>
                        <td>200</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th></th>
                        <th>Total Deduction</th>
                        <th>3300</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div class="full-width">
        <div class="siteInfo">
            <h2 style="margin-bottom: 10px;">Note </h2>
            <p>
                Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web
                designs.
            </p>
        </div>
        <div class="invoiceNum" style="text-align: right;">
            <p style="margin-top: 60px;">
                <b>Total Earnings:</b> 3300<br>
                <b>Total Deductions:</b> 600<br>
            </p>
            <h2>Net Salary 2700</h2>
        </div>
    </div>';

        return $content;
    }
}
