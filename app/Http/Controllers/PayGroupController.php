<?php

namespace App\Http\Controllers;

use App\Http\Requests\Setting\PayGroupRequest;
use App\Http\Resources\PayRoll\PayRollEmployees;
use App\Models\PayGroup;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class PayGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.paygroups.paygroups');
    }

    public function allPaygroups()
    {
        $paygorups = PayGroup::all();
        return response()->json($paygorups);
    }

    public function loadEmployees(PayGroup $paygroup)
    {
        $employees = User::with('employeeType')->where('pay_group_id', $paygroup->id)->get();
        return response()->json($employees);
    }

    public function salaryEmployees(PayGroup $payGroup, Request $request)
    {
        $startDate = new Carbon($request->startDate);
        $endDate = new Carbon($request->endDate);
        $id = $payGroup->id;
        $employees = User::with('employeeType', 'allowances.allowance')->salaryEmployees($id, $startDate, $endDate)->get();
        return response()->json($employees);
    }

    public function salaryGeneratedEmployees(PayGroup $payGroup, Request $request)
    {
        $startDate = new Carbon($request->startDate);
        $endDate = new Carbon($request->endDate);
        $id = $payGroup->id;
        $employees = User::salaryGeneratedEmployees($id, $startDate, $endDate)->get();
        return new PayRollEmployees($employees);
        // return response()->json($employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PayGroupRequest $request)
    {
        try {
            PayGroup::create($request->all());
        } catch (\Exception $e) {
            // throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        return response()->json(['success' => 'Record Added.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PayGroup  $payGroup
     * @return \Illuminate\Http\Response
     */
    public function show(PayGroup $payGroup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PayGroup  $payGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(PayGroupRequest $payGroup)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PayGroup  $payGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayGroup $payGroup)
    {
        try {
            $payGroup->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PayGroup  $payGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayGroup $payGroup)
    {
        //
    }
}
