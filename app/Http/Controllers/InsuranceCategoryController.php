<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InsuranceCategory;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Requests\Setting\InsuranceCategoryRequest;
class InsuranceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.insurance.insuranceCategories');
    }

    public function allInsuranceCategories()
    {
        $categories = InsuranceCategory::all();
        return response()->json($categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InsuranceCategoryRequest $request)
    {
        try{
            InsuranceCategory::create($request->all());
            return response()->json(['success' => 'Record added successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\insuranceCategory  $insuranceCategory
     * @return \Illuminate\Http\Response
     */
    public function show(insuranceCategory $insuranceCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\insuranceCategory  $insuranceCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(insuranceCategory $insuranceCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\insuranceCategory  $insuranceCategory
     * @return \Illuminate\Http\Response
     */
    public function update(InsuranceCategoryRequest $request, insuranceCategory $insuranceCategory)
    {
        try{
            $insuranceCategory->update($request->all());
            return response()->json(['success' => 'Record udpated successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\insuranceCategory  $insuranceCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(insuranceCategory $insuranceCategory)
    {
        //
    }
}
