<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\AssetCondition;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\Setting\AssetConditionRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class AssetConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.assets.assetsConditions');
    }

    public function allConditions()
    {
        $conditions = AssetCondition::all();
        return response()->json($conditions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetConditionRequest $request)
    {
        try{
            AssetCondition::create($request->all());
            return response()->json(['success' => 'Record added successfuly.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
            // throw new HttpResponseException(response("Unknown Error Occured! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetCondition  $assetCondition
     * @return \Illuminate\Http\Response
     */
    public function show(AssetCondition $assetCondition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssetCondition  $assetCondition
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetCondition $assetCondition)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetCondition  $assetCondition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AssetCondition $assetCondition)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetCondition  $assetCondition
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetCondition $assetCondition)
    {
        //
    }
}
