<?php

namespace App\Http\Controllers;

use App\Http\Requests\Setting\JobShiftRequest;
use App\Models\Shift;
use Illuminate\Http\Request;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.jobShifts.jobShifts');
    }

    public function allShifts()
    {
        $shifts = Shift::all();
        return response()->json($shifts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobShiftRequest $request)
    {
        try {
            Shift::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function show(shift $shift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function edit(shift $shift)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function update(JobShiftRequest $request, shift $shift)
    {
        try {
            $shift->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function destroy(shift $shift)
    {
        //
    }
}
