<?php

namespace App\Http\Controllers;

use App\Models\EventDetail;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Setting\EventDetailRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class EventDetailController extends Controller
{
    /**
     * return all events data.
     *
     * @return \Illuminate\Http\Response
     */
    public function allEvents()
    {
        return EventDetail::with('eventType')->get();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.events.eventDisplayIndex');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventDetailRequest $request)
    {
        try{
            EventDetail::create($request->all());
            return response()->json(['success' => 'Record added successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EventDetail  $eventDetail
     * @return \Illuminate\Http\Response
     */
    public function show(EventDetail $eventDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EventDetail  $eventDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(EventDetail $eventDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EventDetail  $eventDetail
     * @return \Illuminate\Http\Response
     */
    public function update(EventDetailRequest $request, EventDetail $eventDetail)
    {
        try{
            $eventDetail->update($request->all());
            return response()->json(['success' => 'Record updated successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EventDetail  $eventDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventDetail $eventDetail)
    {
        //
    }
}
