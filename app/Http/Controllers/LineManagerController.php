<?php

namespace App\Http\Controllers;

use App\Http\Resources\Employee\LineManagerMainResource;
use App\Models\User;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class LineManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.linemanager');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    public function allLineManagers()
    {
        $lineManagers = User::lineManagers();
        return response()->json($lineManagers);
    }

    public function employees($linemanager)
    {
        $employees = User::where('line_manager_id', $linemanager)->get();
        // return $employees;
        return new LineManagerMainResource($employees);
        // return response()->json($employees);
    }

    public function setEmployees($linemanager, Request $request)
    {
        $employees = $request->employees;
        try {
            User::setLinemanager($employees, $linemanager);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LineManager  $lineManager
     * @return \Illuminate\Http\Response
     */
    public function show(LineManager $lineManager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LineManager  $lineManager
     * @return \Illuminate\Http\Response
     */
    public function edit(LineManager $lineManager)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LineManager  $lineManager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LineManager $lineManager)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LineManager  $lineManager
     * @return \Illuminate\Http\Response
     */
    public function destroy(LineManager $lineManager)
    {
        //
    }
}
