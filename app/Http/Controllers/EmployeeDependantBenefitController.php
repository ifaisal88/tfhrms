<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeDependantBenefit;
use App\Http\Requests\EmployeeDependentBenefitRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeDependantBenefitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employeeDependantBenefit  $employeeDependantBenefit
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeDependantBenefit $employeeDependantBenefit)
    {
        return response()->json($employeeDependantBenefit);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeDependantBenefit  $employeeDependantBenefit
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeDependantBenefit $employeeDependantBenefit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeDependantBenefit  $employeeDependantBenefit
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeDependentBenefitRequest $request, EmployeeDependantBenefit $employeeDependantBenefit)
    {
        try{
            $employeeDependantBenefit->update($request->all());
            return response()->json(['success' => 'Dependant benefit updated.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeDependantBenefit  $employeeDependantBenefit
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeDependantBenefit $employeeDependantBenefit)
    {
        //
    }
}
