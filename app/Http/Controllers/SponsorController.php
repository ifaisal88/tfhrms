<?php

namespace App\Http\Controllers;

use App\Models\Sponsor;
use Illuminate\Http\Request;
use App\Http\Requests\Setting\SponsorsRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class SponsorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.sponsors.sponsors');
    }

    public function allSponsors()
    {
        $sponsors = Sponsor::with('company')->get();
        return response()->json($sponsors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SponsorsRequest $request)
    {
        try {
            Sponsor::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknon Error! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        return response()->json(['success' => 'Record Added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function show(Sponsor $sponsor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function edit(Sponsor $sponsor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function update(SponsorsRequest $request, Sponsor $sponsor)
    {
        try {
            $sponsor->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknon Error! Contact Admin", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
        return response()->json(['success' => 'Record Updated.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Sponsor  $sponsor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sponsor $sponsor)
    {
        //
    }
}
