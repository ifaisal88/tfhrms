<?php

namespace App\Http\Controllers;

use App\Models\EmployeeRules;
use Illuminate\Http\Request;

class EmployeeRulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employeeRules  $employeeRules
     * @return \Illuminate\Http\Response
     */
    public function show(employeeRules $employeeRules)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employeeRules  $employeeRules
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeRules $employeeRules)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employeeRules  $employeeRules
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employeeRules $employeeRules)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employeeRules  $employeeRules
     * @return \Illuminate\Http\Response
     */
    public function destroy(employeeRules $employeeRules)
    {
        //
    }
}
