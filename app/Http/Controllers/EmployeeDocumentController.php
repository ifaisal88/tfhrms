<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EmployeeDocument;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

use App\Http\Requests\EmployeeDocumentRequest;
use App\Models\DocumentType;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class EmployeeDocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeDocument  $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeDocument $employeeDocument)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeDocument  $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeDocument $employeeDocument)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeDocument  $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeDocumentRequest $request, EmployeeDocument $employeeDocument)
    {
        try {
            if (!is_null($request->file('document'))) {
                $file = $request->file('document');
                $ext = $file->extension();
                $documentType = DocumentType::find($request->document_type_id);
                $name = $documentType->name . '.' . $ext;
                $content = File::get($file->getRealPath());
                $filePath = $employeeDocument->employee->employee_id . "/" . $name;
                if (Storage::disk('media_documents')->exists($filePath)) {
                    Storage::disk('media_documents')->delete($filePath);
                }
                if (Storage::disk('media_documents')->put($filePath, $content)) {
                    $request->merge(['document_path' => $name]);
                }
            }
            $employeeDocument->update($request->all());
            return response()->json(['success' => 'Bank infromation updated.']);
        } catch (\Exception $e) {
            throw new HttpResponseException(response($e->getMessage(), Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeDocument  $employeeDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeDocument $employeeDocument)
    {
        //
    }
}
