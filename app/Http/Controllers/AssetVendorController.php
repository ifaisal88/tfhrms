<?php

namespace App\Http\Controllers;

use App\Models\AssetVendor;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Requests\Setting\AssetVendorRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class AssetVendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.assets.assetsVendors');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function allVendors()
    {
        $vendors = AssetVendor::get();
        return response()->json($vendors);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssetVendorRequest $request)
    {
        try{
            AssetVendor::create($request->all());
            return response()->json(['success' => 'Record added successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AssetVendor  $assetVendor
     * @return \Illuminate\Http\Response
     */
    public function show(AssetVendor $assetVendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AssetVendor  $assetVendor
     * @return \Illuminate\Http\Response
     */
    public function edit(AssetVendor $assetVendor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AssetVendor  $assetVendor
     * @return \Illuminate\Http\Response
     */
    public function update(AssetVendorRequest $request, AssetVendor $assetVendor)
    {
        try{
            $assetVendor->update($request->all());
            return response()->json(['success' => 'Record updated successfully.']);
        }catch(\Exception $e){
            throw new HttpResponseException(response("Unknown Error Occured! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AssetVendor  $assetVendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(AssetVendor $assetVendor)
    {
        //
    }
}
