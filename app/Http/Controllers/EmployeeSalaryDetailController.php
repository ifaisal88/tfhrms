<?php

namespace App\Http\Controllers;

use App\Models\EmployeeSalary;
use App\Models\EmployeeSalaryDetail;
use Illuminate\Http\Request;

class EmployeeSalaryDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('payroll/payroll-salary');
    }

    public function allPayRolles()
    {
    }

    public function details()
    {
        return view('payroll.payroll-details');
    }

    public function salarySlip($id)
    {
        
    }

    public function storeSalary(Request $request)
    {
        $employeeId = $request->employee_id;

        try {
            $create = EmployeeSalary::create($request->all(
                'employee_id',
                'start_date',
                'end_date',
                'basic_salary',
                'gross_salary',
                'net_salary',
                'working_days',
                'total_days',
                'rate',
                'remarks'
            ));
            
            return response()->json(['success' => 'Salary Generated.']);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\EmployeeSalaryDetail  $employeeSalaryDetail
     * @return \Illuminate\Http\Response
     */
    public function show(EmployeeSalaryDetail $employeeSalaryDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\EmployeeSalaryDetail  $employeeSalaryDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(EmployeeSalaryDetail $employeeSalaryDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\EmployeeSalaryDetail  $employeeSalaryDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeSalaryDetail $employeeSalaryDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\EmployeeSalaryDetail  $employeeSalaryDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeSalaryDetail $employeeSalaryDetail)
    {
        //
    }
}
