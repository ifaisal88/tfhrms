<?php

namespace App\Http\Controllers;

use App\Models\DocumentStatus;
use App\Http\Requests\Setting\DocumentStatusRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class DocumentStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('settings.documentStatuses.documentStatuses');
    }

    public function allDocumentStatus()
    {
        $allStatus = DocumentStatus::all();
        return response()->json($allStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentStatusRequest $request)
    {
        try {
            DocumentStatus::create($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DocumentStatus  $documentStatus
     * @return \Illuminate\Http\Response
     */
    public function show(DocumentStatus $documentStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DocumentStatus  $documentStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(DocumentStatus $documentStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DocumentStatus  $documentStatus
     * @return \Illuminate\Http\Response
     */
    public function update(DocumentStatusRequest $request, DocumentStatus $documentStatus)
    {
        try {
            $documentStatus->update($request->all());
        } catch (\Exception $e) {
            throw new HttpResponseException(response("Unknown Error! Contact Admin.", Response::HTTP_UNPROCESSABLE_ENTITY));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DocumentStatus  $documentStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(DocumentStatus $documentStatus)
    {
        //
    }
}
