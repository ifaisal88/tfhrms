<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class AddEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'        => 'required|alpha',
            'middle_name'       => 'alpha|nullable',
            'last_name'         => 'alpha|nullable',
        //  'email'             => 'required|email|unique:App\Models\User,email',
        //  'home_phone_number' => 'required|numeric',
        //  'emirates_id'       => 'required|alpha_dash|unique:App\Models\User,emirates_id',
        //  'personal_id'       => 'required|alpha_dash|unique:App\Models\User,personal_id',
            'date_of_joining'   => 'required|date',
            'status'            => 'required|between:0,1'
        ];
    }
}
