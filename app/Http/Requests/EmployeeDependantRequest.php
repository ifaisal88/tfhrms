<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeDependantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'relation_id'       => 'required|numeric',
            'country_id'        => 'required|numeric',
            'name'              => 'required|string',
            'date_of_birth'     => 'required|date',
            'passport_number'   => 'nullable|string',
            'passport_expiry'   => 'nullable|date',
            'visa_number'       => 'nullable|string',
            'visa_expiry'       => 'nullable|date',
            'travel'            => 'nullable|between:0,1',
            'insurance'         => 'nullable|between:0,1',
            'remarks'           => 'nullable|string|max:255'
        ];
    }
}
