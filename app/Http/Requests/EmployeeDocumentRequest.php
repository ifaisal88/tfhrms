<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_number' => 'required|alpha_dash',
            'document_status_id' => 'required|numeric',
            'document_type_id' => 'required|numeric',
            'issue_date' => 'required|date',
            'expiry_date' => 'nullable|date|after:issue_date',
            'place_of_issue_country_id' => 'required|numeric',
            'issued_by_country_id' => 'required|numeric',
            'notify_period_in_days' => 'required|numeric',
            'description' => 'nullable|string|max:255',
            'document' => 'nullable|mimes:jpeg,jpg,png,pdf',
        ];
    }

    public function attributes()
    {
        return[
            'document_status_id' => 'document status',
            'document_type_id' => 'document type',
            'place_of_issue_country_id' => 'place of issue',
            'issued_by_country_id' => 'issued by',
        ];
    }
}
