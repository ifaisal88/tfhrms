<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeEducationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qualification_id' => 'required|numeric',
            'institute_id'     => 'required|numeric',
            'country_id'       => 'required|numeric',
            'marks'            => 'required|numeric',
            'completed_date'   => 'nullable|date',
            'description'      => 'nullable|string|max:255',
            'degree'           => 'nullable|mimes:jpeg,jpg,png,pdf'
        ];
    }
}
