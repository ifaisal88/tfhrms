<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class AssetItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                Rule::unique('asset_items')->ignore($this->assetItem),
            ],
            'asset_type_id' => [
                'required',
                'integer',
            ],
            'asset_brand_id' => [
                'required',
                'integer',
            ],
            'asset_condition_id' => [
                'required',
                'integer',
            ],
            'currency_id' => [
                'required',
                'integer',
            ],
            'description' => [
                'nullable',
                'string'
            ],
            'status' => [
                'nullable',
                'boolean'
            ],
            'purchase_price' => [
                'required',
                'integer'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'asset_type_id' => 'type',
            'asset_brand_id' => 'brand',
            'asset_condition_id' => 'condition',
            'asset_vendor_id' => 'vendor',
            'currency_id' => 'currency',
        ];
    }
}
