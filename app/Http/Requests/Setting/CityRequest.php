<?php

namespace App\Http\Requests\setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => [
                'required',
                'integer'
            ],
            'name' => [
                'required',
                'string',
                Rule::unique('cities')->ignore($this->city)
            ],
            'description' => [
                'nullable',
                'string'
            ],
            'status' => [
                'nullable',
                'boolean'
            ]
        ];
    }

    public function attributes()
    {
        return [
            'country_id' => 'country'
        ];
    }
}
