<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeRuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contract_type_id'          => 'required|numeric',
            'notice_period'             => 'required|string',
            'shift_id'                  => 'required|numeric',
            'leave_per_annum_id'        => 'nullable|numeric',
            'gratuity_rule_country_id'  => 'nullable|numeric',
            'ticket_entitlement_id'     => 'nullable|numeric',
            'route_from_id'             => 'nullable|numeric',
            'route_to_id'               => 'nullable|numeric|different:route_from_id',
            'fare_amount'               => 'nullable|numeric',
            'gratuity'                  => 'nullable|between:0,1',
            'air_fare'                  => 'nullable|between:0,1',
            'leave_salary'              => 'nullable|between:0,1',
            'medical_insurance'         => 'nullable|between:0,1',
            'visa_processing_cost'      => 'nullable|between:0,1',
        ];
    }

    public function attributes()
    {
           return [
            'shift_id' => "shift",
            'leave_per_annum_id' => "leave rule",
            'gratuity_rule_country_id' => "gratuity rule",
            'ticket_entitlement_id' => "ticket entitlement",
            'route_from_id' => "route from",
            'route_to_id' => "route to",
           ];
    }
}
