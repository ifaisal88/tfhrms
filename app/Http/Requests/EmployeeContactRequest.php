<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($this->user)
            ],
            'personal_email' => [
                'required',
                'email',
                'different:email',
                Rule::unique('users')->ignore($this->user)
            ],
            'mobile_number' => [
                'required',
                'numeric',
                Rule::unique('users')->ignore($this->user)
            ],
            'home_phone_number' => [
                'required',
                'numeric',
            ],
            'work_mobile' => [
                'nullable',
                'numeric',
                Rule::unique('users')->ignore($this->user)
            ],
            'current_address' => [
                'string',
                'required',
                'max:255'
            ],
            'home_country_address' => [
                'string',
                'required',
                'max:255'
            ]
        ];
    }
}
