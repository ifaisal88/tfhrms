<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeInsuranceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'insurance_provider_id'         => 'required|numeric',
            'insurance_company_id'          => 'required|numeric',
            'policy_number'                 => 'required|string',
            'insurance_category_id'         => 'required|numeric',
            'policy_contract_start_date'    => 'required|date',
            'policy_contract_end_date'      => 'required|date|after:policy_contract_start_date',
            'employee_insured_start_date'   => 'required|date',
            'employee_insured_end_date'     => 'required|date|after:employee_insured_start_date',
            'annual_premium_per_year_rate'  => 'required|numeric',
            'actual_rate'                   => 'required|numeric',
            'monthly_rate'                  => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'insurance_provider_id' => 'insurance provider',
            'insurance_company_id' => 'insurance company',
            'insurance_category_id' => 'insurance category',
        ];
    }
}
