<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmergencyContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name'         => 'required|string',
            'email'             => 'required|email',
            'relation'          => 'required|numeric',
            'mobile_number'     => 'required|numeric',
            'home_phone_number' => 'nullable|numeric',
            'work_number'       => 'nullable|numeric',
            'address'           => 'required|string|max:255',
            'description'       => 'nullable|string|max:255'
        ];
    }
}
