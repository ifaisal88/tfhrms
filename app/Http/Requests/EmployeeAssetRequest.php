<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeAssetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'asset_type_id' => [
                'required',
                'numeric'
            ],
            'asset_brand_id' => [
                'required',
                'numeric'
            ],
            'asset_condition_id' => [
                'required',
                'numeric',
            ],
            'serial_no' => [
                'required',
                'string',
                'max:35',
                Rule::unique('employee_assets')->ignore($this->employeeAsset),
            ],
            'description' => 'nullable|string',
            'issue_date' => 'required|date',
            'return_date' => 'nullable|date|after:issue_date|required_if:returned,true',
            'returned' => 'nullable|boolean'
        ];
    }

    public function attributes()
    {
        return [
            'asset_type_id' => 'asset type',
            'asset_brand_id' => 'asset brand',
            'asset_condition_id' => 'asset condition',
            'currency_id' => 'currency',
        ];
    }
}
