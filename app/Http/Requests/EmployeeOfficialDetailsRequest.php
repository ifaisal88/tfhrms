<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\Exceptions\HttpResponseException;

class EmployeeOfficialDetailsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'position_category_id'  => 'nullable|numeric',
            'designation_id'        => 'required|numeric',
            'department_id'         => 'required|numeric',
            'sub_department_id'     => 'required|numeric',
            'job_country_id'        => 'required|numeric',
            'job_city_id'           => 'required|numeric',
            'date_of_joining'       => 'required|date',
            'probation_period'      => 'required|numeric',
            // 'cost_center_id'        => 'required|numeric',
            // 'project_id'            => 'nullable|numeric',
            'supervisor'            => 'required|between:0,1',
            'date_of_exit'          => 'nullable|date'
        ];
    }
}
