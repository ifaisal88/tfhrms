<?php

namespace App\Http\Resources\Employee\Tabs;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeDependentBenifitTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? $this->id : '',
            'employeeDependant' => [
                'id' => isset($this->employeeDependant->id) ? $this->employeeDependant->id : '',
                'name' => isset($this->employeeDependant->name) ? $this->employeeDependant->name : '',
            ],
            'ticketEntitlement' => [
                'id' => isset($this->ticketEntitlement->id) ? $this->ticketEntitlement->id : '',
                'name' => isset($this->ticketEntitlement->name) ? $this->ticketEntitlement->name : '',
            ],
            'routeFrom' => [
                'id' => isset($this->routeFrom->id) ? $this->routeFrom->id : '',
                'name' => isset($this->routeFrom->name) ? $this->routeFrom->name : '',
            ],
            'routeTo' => [
                'id'   => isset($this->routeTo->id) ? $this->routeTo->id : '',
                'name'   => isset($this->routeTo->name) ? $this->routeTo->name : '',
            ],
            'insurance' => $this->employeeDependant->insurance?true:false,
            'travelAllowance' => $this->employeeDependant->travel_allounce?true:false,
            'insuranceCompany' => [
                'id' => isset($this->insuranceCompany->id) ? $this->insuranceCompany->id : '',
                'name' => isset($this->insuranceCompany->name) ? $this->insuranceCompany->name : '',
            ],
            'category' => [
                'id' => isset($this->insuranceCategory->id) ? $this->insuranceCategory->id : '',
                'name' => isset($this->insuranceCategory->name) ? $this->insuranceCategory->name : '',
            ],
            'currency' => [
                'id' => isset($this->currency->id) ? $this->currency->id : '',
                'name' => isset($this->currency->name) ? $this->currency->name : '',
            ],
            'dateFrom' => isset($this->start) ? $this->start : '',
            'dateTo' => isset($this->end) ? $this->end : '',
            'policyNumber' => isset($this->policy_number) ? $this->policy_number : '',
            'fareAmount' => isset($this->fare_amount) ? $this->fare_amount : '',
            'returnTicket' => $this->return_ticket?true:false,
            'insuranceAmount' => isset($this->insurance_amount) ? $this->insurance_amount : '',
            'premium' => isset($this->premium) ? $this->premium : '',
            'description' => isset($this->description) ? $this->description : '',
            'status' => isset($this->status) ? $this->status : '',
        ];
    }
}
