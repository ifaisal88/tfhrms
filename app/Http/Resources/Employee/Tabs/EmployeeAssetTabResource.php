<?php

namespace App\Http\Resources\Employee\Tabs;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeAssetTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'selected' => false,
            'id' => $this->id,
            'assetType' => [
                'id' => isset($this->assetType->id) ? $this->assetType->id : '',
                'name' => isset($this->assetType->name) ? $this->assetType->name : '',
            ],
            'brand' => [
                'id' => isset($this->assetBrand->id) ? $this->assetBrand->id : '',
                'name' => isset($this->assetBrand->name) ? $this->assetBrand->name : '',
            ],
            'condition' => [
                'id' => isset($this->assetCondition->id) ? $this->assetCondition->id : '',
                'name' => isset($this->assetCondition->name) ? $this->assetCondition->name : '',
            ],
            'item' => [
                'id' => isset($this->assetItem->id) ? $this->assetItem->id : '',
                'name' => isset($this->assetItem->name) ? $this->assetItem->name : '',
            ],
            'currency' => [
                'id' => isset($this->assetItem) ? $this->assetItem->currency->id : '',
                'name' => isset($this->assetItem) ? $this->assetItem->currency->name : '',
                'symbol' => isset($this->assetItem) ? $this->assetItem->currency->symbol : '',
            ],
            'serial' => $this->serial_no,
            'value' => isset($this->assetItem) ? $this->assetItem->sale_price : '',
            'issuedDate' => isset($this->issue_date) ? $this->issue_date : '',
            'description' => isset($this->description) ? $this->description : '',
            'returnDescription' => isset($this->return_description) ? $this->return_description : '',
            'returnDate' => isset($this->return_date) ? $this->return_date : '',
            'returned' => $this->returned,
            'assignImages' => $this->assigning_images,
            'returnImages' => $this->return_images,
        ];
        // return parent::toArray($request);
    }
}
