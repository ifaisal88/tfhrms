<?php

namespace App\Http\Resources\Employee\Tabs;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeDocumentTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id)?$this->id:'',
            'documentNumber' => isset($this->document_number)?$this->document_number:'',
            'status' => [
                'id' => isset($this->documentStatus->id)?$this->documentStatus->id:'',
                'name' => isset($this->documentStatus->name)?$this->documentStatus->name:'',
            ],
            'documentType' => [
                'id' => isset($this->documentType->id)?$this->documentType->id:'',
                'name' => isset($this->documentType->name)?$this->documentType->name:'',
            ], 
            'placeOfIssue' => [
                'id' => isset($this->placeOfIssue->id)?$this->placeOfIssue->id:'',
                'name' => isset($this->placeOfIssue->name)?$this->placeOfIssue->name:'',
            ],
            'issuedBy' => [
                'id' => isset($this->issuedBy->id)?$this->issuedBy->id:'',
                'name' => isset($this->issuedBy->name)?$this->issuedBy->name:'',
            ],
            'issueDate' => isset($this->issue_date)?$this->issue_date:'',
            'expiryDate' => isset($this->expiry_date)?$this->expiry_date:'',
            'notify' => isset($this->notify_period_in_days)?$this->notify_period_in_days:'',
            'document' => isset($this->document_path)?$this->document_path:'',
            'description' => isset($this->description)?$this->description:'',
            'documentPath' => $this->path,
        ];
        // return parent::toArray($request);
    }
}
