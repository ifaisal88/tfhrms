<?php

namespace App\Http\Resources\Employee\Tabs;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeBankInfoTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id' => isset($this->id)?$this->id:'',
            'bank' => [
                'id' => isset($this->bank->id)?$this->bank->id:'',
                'name' => isset($this->bank->name)?$this->bank->name:'',
            ],
            'accountNumber' => isset($this->account_no)?$this->account_no:'',
            'iban' => isset($this->iban_no)?$this->iban_no:'',
            'accountHolder' => isset($this->account_holdar_name)?$this->account_holdar_name:'',
            'description' => isset($this->description)?$this->description:'',
            'status' => isset($this->status)?$this->status:'',
        ];
    }
}
