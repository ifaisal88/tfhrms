<?php

namespace App\Http\Resources\Employee\Tabs;

use Illuminate\Http\Resources\Json\JsonResource;

class EmergencyContactTabResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? $this->id : '',
            'name' => isset($this->full_name) ? $this->full_name : '',
            'email' => isset($this->email) ? $this->email : '',
            'relation' => [
                'id' => isset($this->relationName->id) ? $this->relationName->id : '',
                'name' => isset($this->relationName->name) ? $this->relationName->name : '',
            ],
            'mobileNumber' => isset($this->mobile_number) ? $this->mobile_number : '',
            'homeNumber' => isset($this->home_phone_number) ? $this->home_phone_number : '',
            'workNumber' => isset($this->work_number) ? $this->work_number : '',
            'address' => isset($this->address) ? $this->address : '',
            'description' => isset($this->description) ? $this->description : ''
        ];
    }
}
