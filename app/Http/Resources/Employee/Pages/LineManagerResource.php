<?php

namespace App\Http\Resources\Employee\Pages;

use Illuminate\Http\Resources\Json\JsonResource;

class LineManagerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'value' => $this->employee_id,
            'text' => $this->full_name.' ('.$this->employee_id.')',
        ];
        // return parent::toArray($request);
    }
}
