<?php

namespace App\Http\Resources\Employee;

use Illuminate\Support\Collection;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Employee\Tabs\DependentTabResource;
use App\Http\Resources\Employee\Tabs\EmergencyContactTabResource;
use App\Http\Resources\Employee\Tabs\EducationTabResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'mainTab' => [
                'linemanager' => [
                    'id' => isset($this->linemanager->employee_id) ? $this->linemanager->employee_id : '',
                    'name' => isset($this->linemanager->full_name) ? $this->linemanager->full_name : '',
                ],
                'company' => [
                    'id' => isset($this->company->id) ? $this->company->id : '',
                    'name' => isset($this->company->name) ? $this->company->name : '',
                ],
                'sponsor' => [
                    'id' => isset($this->sponsor->id) ? $this->sponsor->id : '',
                    'name' => isset($this->sponsor->name) ? $this->sponsor->name : '',
                ],
                'payGroup' => [
                    'id' => isset($this->payGroup->id) ? $this->payGroup->id : '',
                    'name' => isset($this->payGroup->name) ? $this->payGroup->name : '',
                ]
            ],
            'personalTab' => [
                'nationality' => [
                    'id' => isset($this->nationality->id) ? $this->nationality->id : '',
                    'name' => isset($this->nationality->name) ? $this->nationality->name : '',
                ],
                'religion' => [
                    'id' => isset($this->religion->id) ? $this->religion->id : '',
                    'name' => isset($this->religion->name) ? $this->religion->name : '',
                ],
                'maritialStatus' => isset($this->maritial_status) ? $this->maritial_status : '',
                'bloodGroup' => isset($this->blood_group) ? $this->blood_group : '',
                'birthDate' => isset($this->date_of_birth) ? $this->date_of_birth : '',
                'maidenName' => isset($this->maiden_name) ? $this->maiden_name : '',
                'fatherMoter' => isset($this->parent_name) ? $this->parent_name : '',
                'gender' => isset($this->gender) ? $this->gender : '',
            ],
            'employeeContactTab' => [
                'companyEmail' => isset($this->email) ? $this->email : '',
                'personalEmail' => isset($this->personal_email) ? $this->personal_email : '',
                'mobileNumber' => isset($this->mobile_number) ? $this->mobile_number : '',
                'homeNumber' => isset($this->home_phone_number) ? $this->home_phone_number : '',
                'workMobile' => isset($this->work_mobile) ? $this->work_mobile : '',
                'currentAddress' => isset($this->current_address) ? $this->current_address : '',
                'homeCountryAddress' => isset($this->home_country_address) ? $this->home_country_address : '',
            ],
            'dependentTab' => DependentTabResource::collection(isset($this->dependants) ? $this->dependants : []),
            'emergencyContactTab' => EmergencyContactTabResource::collection(isset($this->contactPeoples) ? $this->contactPeoples : []),
            'educationTab' => EducationTabResource::collection(isset($this->educations) ? $this->educations : []),
        ];
        // return parent::toArray($request);
    }
}
