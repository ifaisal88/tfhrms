<?php

namespace App\Http\Resources\PayRoll;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class SlipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $startDate = new Carbon($this->start_date);
        return[
            'id' => $this->id,
            'employeeName' => $this->employee->full_name,
            'employeeId' => $this->employee->employee_id,
            'employeeDesignation' => !is_null($this->employee->designation)?$this->employee->designation->name:"",
            'profilePic' => $this->employee->path,
            'basicSalary' => $this->basic_salary,
            'grossSalary' => $this->gross_salary,
            'allowancesTotal' => $this->allowances_total,
            'deductablesTotal' => $this->deductable_total,
            'grossSalary' => $this->gross_salary,
            'netSalary' => $this->net_salary,
            'salaryMonth' => $startDate->format('F').', '.$startDate->year,
            'allowances' => $this->allowances_total > 0 ? collect(EmployeeSalaryAllowancesResource::collection($this->details))->filter(function($value, $key){
                return $value != null;
            })->toArray():null,
            'deductables' => $this->deductable_total > 0 ? collect(EmployeeSalaryDeductionsResource::collection($this->details))->filter(function($value, $key){
                return $value != null;
            })->toArray() : null,
        ];
    }
}
