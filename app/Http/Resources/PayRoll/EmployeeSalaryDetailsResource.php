<?php

namespace App\Http\Resources\PayRoll;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeSalaryDetailsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'salaryId' => $this->employeeSalary->id,
            'selected' => false,
            'name' => [
                'id' => $this->employee_id,
                'name' => $this->full_name,
                'pic' => $this->path,
            ],
            'basicSalary' => $this->basic_salary,
            'type' => isset($this->employeeType) ? $this->employeeType->name : "",
            'laborCardNumber' => $this->labor_card_number,
            'personCode' => $this->person_code,
            'dateOfJoining' => $this->date_of_joining,
            'salaryDate' => [
                'startDate' => $this->employeeSalary->start_date,
                'endDate' => $this->employeeSalary->end_date,
            ],
            'allowancesTotal' => $this->employeeSalary->allowances_total,
            'grossSalary' => $this->employeeSalary->gross_salary,
            'deductablesTotal' => $this->employeeSalary->deductable_total,
            'netSalary' => $this->employeeSalary->net_salary,
            'actions' => $this->employeeSalary->id,
            'selected' => false,
            'allowances' => $this->employeeSalary->allowances_total > 0 ? collect(EmployeeSalaryAllowancesResource::collection($this->employeeSalary->details))->filter(function ($value, $key) {
                return $value != null;
            })->toArray() : null,
            'deductables' => $this->employeeSalary->deductable_total > 0 ? collect(EmployeeSalaryDeductionsResource::collection($this->employeeSalary->details))->filter(function ($value, $key) {
                return $value != null;
            })->toArray() : null,
        ];
    }
}
