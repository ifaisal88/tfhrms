<?php

namespace App\Http\Resources\PayRoll;

use Illuminate\Http\Resources\Json\JsonResource;

class EmployeeSalaryAllowancesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if (!is_null($this->allowance)) {
            return [
                'id' => $this->id,
                'allowanceId' => $this->allowance->id,
                'slug' => $this->allowance->slug,
                'name' => $this->allowance->name,
                'amount' => $this->amount,
                'remarks' => $this->remarks,
            ];
        }
    }
}
