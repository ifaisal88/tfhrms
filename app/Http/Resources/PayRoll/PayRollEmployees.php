<?php

namespace App\Http\Resources\PayRoll;

use Illuminate\Http\Resources\Json\JsonResource;

class PayRollEmployees extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return EmployeeSalaryDetailsResource::collection($this);
    }
}
