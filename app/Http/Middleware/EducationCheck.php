<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EducationCheck
{
    protected $user;
    public function __construct()
    {
        $this->user = \Auth::user();
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $permissions = $this->user->role->permissions;
        $modulePermissions = $permissions->where('module', 'Education');
        $route = $request->route()->getName();
        $permission = explode('.', $route)[1];
        $count = $modulePermissions->where('name', $permission)->count();
        if ($count > 0) {
            return $next($request);
        } else {
            return response()->view('components.page-403');
        }
    }
}
