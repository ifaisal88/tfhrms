<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class SalaryAllowanceExport implements FromArray, WithHeadings, ShouldAutoSize, WithStyles
{
    /**
     * @return \Illuminate\Support\Collection
     */
    protected $data, $allowances, $headings;

    public function __construct(array $data, $allowances)
    {
        $this->data = $data;
        $this->allowances = $allowances;
        $this->headings = [];
    }

    public function headings(): array
    {
        $this->headings = [
            'Name',
            'Employee Id',
            'Emirates Id',
            'Person Code',
            'Joining Date',
            'Start Date',
            'End Date',
        ];
        foreach ($this->allowances as $allowance) {
            $this->headings[] = $allowance->name;
            if($allowance->enable_remarks == 1){
                $this->headings[] = $allowance->name.' Remarks';
            }
        }
        return $this->headings;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true, 'size' => 15]],
        ];
    }
}
