<?php

namespace App\Exports;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class AttendanceHistoryExport implements FromCollection, WithHeadings, ShouldAutoSize, WithStyles
// class AttendanceHistoryExport implements FromArray, WithHeadings
{
    protected $atendances, $startDate, $endDate, $headings;

    public function __construct(array $atendances, $startDate, $endDate)
    {
        $this->startDate = new Carbon($startDate);
        $this->endDate = new Carbon($endDate);
        $this->atendances = $atendances;
        $this->headings = [];
    }

    public function headings(): array
    {
        $this->headings = [
            'Name',
            'Employee Id',
            'Emirates Id',
        ];
        $period = CarbonPeriod::create($this->startDate, $this->endDate);
        foreach ($period as $date) {
            $this->headings[] = $date->format('Y-m-d');
        }
        return $this->headings;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => ['font' => ['bold' => true, 'size' => 15]],
        ];
        // for ($i = 1; $i < count($this->headings); $i++) {
        //     if ($i > 3) {
        //         $j = $i + 4;
        //         $startColumn = $sheet->getCellByColumnAndRow($i, 1)->getColumn();
        //         $endColumn = $sheet->getCellByColumnAndRow($j, 1)->getColumn();
        //         $sheet->mergeCells($startColumn . "1:" . $endColumn . "1");
        //     }
        // }
    }

    // public function registerEvents(): array
    // {
    //     return [
    //         AfterSheet::class    => function(AfterSheet $event) {
    //             // $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);

    //             $event->sheet->mergeCells('A1:D1');
    //         },
    //     ];
    // }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return collect($this->atendances);
    }
}
