<?php

namespace App\Observers;

use App\Models\InsuranceClaim;
use App\Models\InsuranceClaimDetail;
use Carbon\Carbon;
use DB;

class InsuranceClaimObserver
{
    /**
     * Handle the InsuranceClaim "creating" event.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return void
     */
    public function creating(InsuranceClaim $insuranceClaim)
    {
        DB::beginTransaction();
    }
    /**
     * Handle the InsuranceClaim "created" event.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return void
     */
    public function created(InsuranceClaim $insuranceClaim)
    {
        if (!is_null(request()->details)) {
            $data = [];
            try {
                foreach (request()->details as $benefit) {
                    $data[] = [
                        'insurance_claim_id' => $insuranceClaim->id,
                        'insurance_benefit_id' => $benefit['benefitId'],
                        'amount' => $benefit['amount'],
                        'created_at' => new Carbon(),
                        'updated_at' => new Carbon(),
                    ];
                }
                InsuranceClaimDetail::insert($data);
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                throw new \Exception($e->getMessage(), 1);
            }
        }
    }

    /**
     * Handle the InsuranceClaim "updating" event.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return void
     */
    public function updating(InsuranceClaim $insuranceClaim)
    {
        DB::beginTransaction();
    }
    /**
     * Handle the InsuranceClaim "updated" event.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return void
     */
    public function updated(InsuranceClaim $insuranceClaim)
    {
        if (!is_null(request()->details)) {
            try {
                foreach (request()->details as $benefit) {
                    InsuranceClaimDetail::updateOrCreate(['id' => $benefit['id']], [
                        'amount' => $benefit['amount']
                    ]);
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                throw new \Exception($e->getMessage(), 1);
            }
        }
    }

    /**
     * Handle the InsuranceClaim "deleted" event.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return void
     */
    public function deleted(InsuranceClaim $insuranceClaim)
    {
        //
    }

    /**
     * Handle the InsuranceClaim "restored" event.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return void
     */
    public function restored(InsuranceClaim $insuranceClaim)
    {
        //
    }

    /**
     * Handle the InsuranceClaim "force deleted" event.
     *
     * @param  \App\Models\InsuranceClaim  $insuranceClaim
     * @return void
     */
    public function forceDeleted(InsuranceClaim $insuranceClaim)
    {
        //
    }
}
