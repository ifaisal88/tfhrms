<?php

namespace App\Observers;

use App\Models\Allowance;
use App\Models\EmployeeAllowance;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;

use function PHPUnit\Framework\throwException;

class EmployeeObserver
{
    /**
     * Handle the User "creating" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function creating(User $user)
    {
        $user->full_name = $user->first_name . ' ' . $user->middle_name . ' ' . $user->last_name;
        if (!isset($user->employee_id)) {
            $count = str_pad(User::count(), 5, '0', STR_PAD_LEFT);
            $empId = str_replace("-", "", $user->date_of_joining) . $count;
            $empId++;
            $user->employee_id = $empId;
        }
    }
    /**
     * Handle the User "created" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function created(User $user)
    {
        //
    }

    /**
     * Handle the User "updating" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updating(User $user)
    {
        if (isset(request()->contract_type_id)) {
            if ($user->contractType->is_permanent) {
                $this->setAllowances($user);
            } else {
                EmployeeAllowance::where('user_employee_id', $user->employee_id)->delete();
            }
        }
        $user->full_name = $user->first_name . ' ' . $user->middle_name . ' ' . $user->last_name;
        // $count = str_pad(User::count(), 5, '0', STR_PAD_LEFT);
        // $empId = str_replace("-", "", $user->date_of_joining).$count;
        // $empId++;
        // $user->employee_id = $empId;
    }
    /**
     * Handle the User "updated" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function updated(User $user)
    {
    }

    /**
     * Handle the User "deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        //
    }

    /**
     * Handle the User "restored" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     *
     * @param  \App\Models\User  $user
     * @return void
     */
    public function forceDeleted(User $user)
    {
        //
    }

    protected function setAllowances($user, $allowances = null)
    {
        $allowances = Allowance::defaultAllowances()->get();
        $startDate = new Carbon();
        $endDate = $startDate->copy()->addYear();
        foreach ($allowances as $allowance) {
            try {
                EmployeeAllowance::updateOrCreate(
                    [
                        'user_employee_id' => $user->employee_id,
                        'allowance_id' => $allowance->id,
                    ],
                    [
                        'user_employee_id' => $user->employee_id,
                        'allowance_id' => $allowance->id,
                        'remarks' => '',
                        'type' => '',
                        'amount' => 0,
                        'status' => true,
                        'start_date' => $startDate,
                        'end_date' => $endDate,
                        'created_at' => $startDate,
                        'updated_at' => $startDate,
                        'created_by' => \Auth::user()->employee_id,
                        'updated_by' => \Auth::user()->employee_id,
                    ]
                );
            } catch (\Exception $e) {
                throw new \Exception($e->getMessage());
                break;
            }
        }
    }
}
