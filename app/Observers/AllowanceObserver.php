<?php

namespace App\Observers;

use App\Models\Allowance;

class AllowanceObserver
{
    /**
     * Handle the Allowance "created" event.
     *
     * @param  \App\Models\Allowance  $allowance
     * @return void
     */
    public function creating(Allowance $allowance)
    {
        $allowance->slug = str_replace(" ", "-", strtolower($allowance->name));
    }

    /**
     * Handle the Allowance "created" event.
     *
     * @param  \App\Models\Allowance  $allowance
     * @return void
     */
    public function created(Allowance $allowance)
    {
        //
    }

    /**
     * Handle the Allowance "updated" event.
     *
     * @param  \App\Models\Allowance  $allowance
     * @return void
     */
    public function updating(Allowance $allowance)
    {
        $allowance->slug = str_replace(" ", "-", strtolower($allowance->name));
    }

    /**
     * Handle the Allowance "updated" event.
     *
     * @param  \App\Models\Allowance  $allowance
     * @return void
     */
    public function updated(Allowance $allowance)
    {
        //
    }

    /**
     * Handle the Allowance "deleted" event.
     *
     * @param  \App\Models\Allowance  $allowance
     * @return void
     */
    public function deleted(Allowance $allowance)
    {
        //
    }

    /**
     * Handle the Allowance "restored" event.
     *
     * @param  \App\Models\Allowance  $allowance
     * @return void
     */
    public function restored(Allowance $allowance)
    {
        //
    }

    /**
     * Handle the Allowance "force deleted" event.
     *
     * @param  \App\Models\Allowance  $allowance
     * @return void
     */
    public function forceDeleted(Allowance $allowance)
    {
        //
    }
}
