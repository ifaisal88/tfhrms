<?php

namespace App\Observers;

use App\Models\EmployeeLeave;
use App\Models\EmployeeLeaveStatus;
use App\Models\Notification;
use Carbon\Carbon;
use DB;

class EmployeeLeaveObserver
{

    /**
     * Handle the Allowance "creating" event.
     *
     * @param  \App\Models\EmployeeLeave  $allowance
     * @return void
     */
    public function creating(EmployeeLeave $employeeLeave)
    {
        DB::begintransaction();
    }
    /**
     * Handle the EmployeeLeave "created" event.
     *
     * @param  \App\Models\EmployeeLeave  $employeeLeave
     * @return void
     */
    public function created(EmployeeLeave $employeeLeave)
    {
        try {
            $data = [
                'employee_leave_id' => $employeeLeave->id,
                'status' => false,
                'submitted_date' => new Carbon(),
                'pending_with_user_id' => \Auth::user()->employee_id,
            ];
            EmployeeLeaveStatus::create($data);
            $linemanager = $employeeLeave->employee->line_manager_id;
            Notification::create([
                'user_employee_id' => $employeeLeave->employee_id,
                'type' => 'leave',
                'notification' => 'Your leave request is submitted successfully.',
                'status' => false,
            ]);
            if (!is_null($linemanager)) {
                Notification::create([
                    'user_employee_id' => $linemanager,
                    'type' => 'leave',
                    'notification' => $employeeLeave->employee->full_name. '('.$employeeLeave->employee_id.')'.' has requested for leave.',
                    'status' => false,
                ]);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw new \Exception($e->getMessage(), 1);
        }
    }

    /**
     * Handle the EmployeeLeave "updating" event.
     *
     * @param  \App\Models\EmployeeLeave  $employeeLeave
     * @return void
     */
    public function updating(EmployeeLeave $employeeLeave)
    {
        DB::begintransaction();
    }

    /**
     * Handle the EmployeeLeave "updated" event.
     *
     * @param  \App\Models\EmployeeLeave  $employeeLeave
     * @return void
     */
    public function updated(EmployeeLeave $employeeLeave)
    {
        try {
            if (request()->approve) {
                $status = EmployeeLeaveStatus::where('employee_leave_id', $employeeLeave->id)->first();
                $status->update([
                    'status' => true,
                    'approved_by_user_id' => \Auth::user()->employee_id,
                    'pending_with_user_id' => null,
                    'approved_date' => new Carbon()
                ]);
            }
            Notification::create([
                'user_employee_id' => $employeeLeave->employee_id,
                'type' => 'leave',
                'notification' => 'Your leave request is accepted.',
                'status' => false,
            ]);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }
    }

    /**
     * Handle the EmployeeLeave "deleted" event.
     *
     * @param  \App\Models\EmployeeLeave  $employeeLeave
     * @return void
     */
    public function deleted(EmployeeLeave $employeeLeave)
    {
        //
    }

    /**
     * Handle the EmployeeLeave "restored" event.
     *
     * @param  \App\Models\EmployeeLeave  $employeeLeave
     * @return void
     */
    public function restored(EmployeeLeave $employeeLeave)
    {
        //
    }

    /**
     * Handle the EmployeeLeave "force deleted" event.
     *
     * @param  \App\Models\EmployeeLeave  $employeeLeave
     * @return void
     */
    public function forceDeleted(EmployeeLeave $employeeLeave)
    {
        //
    }
}
