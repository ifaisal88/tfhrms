<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmployeeBankDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'bank_id',
        'account_no',
        'iban_no',
        'account_holdar_name',
        'description',
        'status',
        'created_by',
        'updated_by',
    ];

    public function employee()
    {
        return $this->belongsTo(User::class);
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

    public static function boot()
    {
        parent::boot();

        self::updated(function ($model) {
            if ($model->status) {
                try {
                    EmployeeBankDetail::where('id', '!=', $model->id)->update(['status' => false]);
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage(), 1);
                }
            }
        });

        self::created(function ($model) {
            if ($model->status) {
                try {
                    EmployeeBankDetail::where('id', '!=', $model->id)->update(['status' => false]);
                } catch (\Exception $e) {
                    throw new \Exception($e->getMessage(), 1);
                }
            }
        });
    }
}
