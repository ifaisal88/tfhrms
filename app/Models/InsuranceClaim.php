<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InsuranceClaim extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'total_amount',
        'currency_id',
        'description',
        'bill_date',
        'status',
    ];

    public function employee()
    {
        return $this->belongsTo(User::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function details()
    {
        return $this->hasMany(InsuranceClaimDetail::class);
    }
}
