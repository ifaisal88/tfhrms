<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    protected $fillable = [
        'country_id',
        'name',
        'description',
        'status',
        'created_by',
        'updated_by',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function companyEmployees()
    {
        return $this->hasMany(User::class);
    }

    public function sponsors()
    {
        return $this->hasMany(Sponsor::class);
    }

    public function payGroup()
    {
        return $this->hasMany(PayGroup::class);
    }
}
