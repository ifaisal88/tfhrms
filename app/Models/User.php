<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    public $incrementing = false;

    protected $primaryKey = 'employee_id';

    protected $fillable = [
        'employee_id',
        'personal_id',
        'emirates_id',
        'labor_card_number',
        'person_id',
        'person_code',
        'employee_type_id',
        'visa_status_id',
        'employee_category_id',
        'employee_status_id',
        'line_manager_id',
        'company_id',
        'sponsor_id',
        'pay_group_id',
        'nationality_id',
        'religion_id',
        'department_id',
        'sub_department_id',
        'designation_id',
        'created_by',
        'updated_by',
        'maritial_status',
        'date_of_birth',
        'blood_group',
        'maiden_name',
        'gender',
        'first_name',
        'middle_name',
        'last_name',
        'full_name',
        'email',
        'profile_image',
        'password',
        'personal_email',
        'mobile_number',
        'home_phone_number',
        'work_mobile',
        'current_address',
        'home_country_address',
        'date_of_joining',
        'date_of_exit',
        'status',
        'parent_name',
        'probation_period',
        'probation_end_date',
        'project_id',
        'cost_center_id',
        'supervisor',
        'position_category_id',
        'salary_rule_id',
        'salary_currency_id',
        'payment_type_id',
        'last_salary_revision_date',
        'company_account_id',
        'basic_salary',
        'house_allounce',
        'insentives',
        'transport_allounce',
        'mobile_allounce',
        'other_allounce',
        'insurance_provider_id',
        'insurance_company_id',
        'policy_number',
        'insurance_category_id',
        'policy_contract_start_date',
        'policy_contract_end_date',
        'employee_insured_start_date',
        'employee_insured_end_date',
        'annual_premium_per_year_rate',
        'actual_rate',
        'monthly_rate',
        'job_country_id',
        'shift_id',
        'job_city_id',
        'route_from_id',
        'route_to_id',
        'contract_type_id',
        'leave_per_annum_id',
        'gratuity_rule_country_id',
        'air_fare',
        'medical_insurance',
        'ticket_entitlement_id',
        'notice_period',
        'leave_salary',
        'gratuity',
        'visa_processing_cost',
        'max_fair_rates',
        'role_id'
    ];

    protected $appends = ['path', 'modules', 'permissions'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getPathAttribute()
    {
        $exists = Storage::disk('media_images')->has($this->employee_id . '/profile.jpg') ? true : false;
        $path = $exists ? asset('media_images/' . $this->employee_id . '/profile.jpg') : asset('media_images/profile.jpg');
        return $path;
    }

    public function companyAccount()
    {
        return $this->belongsTo(Account::class, 'company_account_id', 'id');
    }

    public function insuranceProvider()
    {
        return $this->belongsTo(InsuranceProvider::class, 'insurance_provider_id', 'id');
    }

    public function insuranceCompany()
    {
        return $this->belongsTo(InsuranceCompany::class, 'insurance_company_id', 'id');
    }

    public function insuranceCategory()
    {
        return $this->belongsTo(InsuranceCategory::class, 'insurance_category_id', 'id');
    }

    public function salaryCurrency()
    {
        return $this->belongsTo(Currency::class, 'salary_currency_id', 'id');
    }

    public function salaryRule()
    {
        return $this->belongsTo(SalaryRule::class);
    }

    public function paymentType()
    {
        return $this->belongsTo(PaymentType::class);
    }

    public function leavePerAnnum()
    {
        return $this->belongsTo(LeavePerAnnum::class);
    }

    public function ticketEntitlement()
    {
        return $this->belongsTo(TicketEntitlement::class);
    }

    public function graduityRuleCountry()
    {
        return $this->belongsTo(GratuityRule::class, 'gratuity_rule_country_id', 'id');
    }

    public function scopeActiveUsers($query)
    {
        $query = $query->where('status', true)->get();
        return $query;
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id')->with('permissions');
    }

    public function getModulesAttribute()
    {
        $modulesId = isset($this->role->permissions) ? $this->role->permissions->pluck('module_id')->unique():[];
        $modules = count($modulesId) > 0?Module::whereIn('id', $modulesId)->pluck('slug')->toArray():$modulesId;
        return $modules;
    }

    public function getPermissionsAttribute()
    {
        $moduels = [];
        if(isset($this->role->permisssions))
        foreach ($this->role->permissions as $permission) {
            $moduels[$permission->module->name][] = $permission->name;
        }
        return $moduels;
    }

    public function employeeType()
    {
        return $this->belongsTo(EmployeeType::class, 'employee_type_id');
    }

    public function employeeCategory()
    {
        return $this->belongsTo(EmployeeCategory::class);
    }

    public function positionCategory()
    {
        return $this->belongsTo(PositionCategory::class);
    }

    public function employeeStatus()
    {
        return $this->belongsTo(EmployeeStatus::class);
    }

    public function employeeSalary()
    {
        return $this->belongsTo(EmployeeSalary::class, 'employee_id', 'employee_id');
    }

    public function linemanager()
    {
        return $this->belongsTo(User::class, 'line_manager_id', 'employee_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function sponsor()
    {
        return $this->belongsTo(Sponsor::class);
    }

    public function payGroup()
    {
        return $this->belongsTo(PayGroup::class);
    }

    public function nationality()
    {
        return $this->belongsTo(Country::class, 'nationality_id', 'id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'job_country_id', 'id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'job_city_id', 'id');
    }

    public function costCenter()
    {
        return $this->belongsTo(CostCenter::class);
    }

    public function religion()
    {
        return $this->belongsTo(Religion::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function subDepartment()
    {
        return $this->belongsTo(SubDepartment::class);
    }

    public function designation()
    {
        return $this->belongsTo(Designation::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function visaStatus()
    {
        return $this->belongsTo(VisaStatus::class);
    }

    public function dependants()
    {
        return $this->hasMany(EmployeeDependant::class);
    }

    public function educations()
    {
        return $this->hasMany(EmployeeEducation::class);
    }

    public function officalDetails()
    {
        return $this->hasMany(EmployeeOfficialDetail::class);
    }

    public function payElements()
    {
        return $this->hasMany(EmployeePayElement::class);
    }

    public function provisions()
    {
        return $this->hasMany(EmployeeProvision::class);
    }

    public function dependantBenefits()
    {
        return $this->hasMany(EmployeeDependantBenefit::class);
    }

    public function insurance()
    {
        return $this->hasMany(EmployeeInsurance::class);
    }

    public function documents()
    {
        return $this->hasMany(EmployeeDocument::class);
    }

    public function assets()
    {
        return $this->hasMany(EmployeeAsset::class);
    }

    public function bankDetails()
    {
        return $this->hasMany(EmployeeBankDetail::class);
    }

    public function contactPeoples()
    {
        return $this->hasMany(ContactPeople::class);
    }

    public function employeeBankDetail()
    {
        return $this->hasMany(EmployeeBankDetail::class);
    }

    public function contractType()
    {
        return $this->belongsTo(ContractType::class);
    }

    public function employeeDocuments()
    {
        return $this->hasMany(DocumentType::class);
    }
    /**
     * Get the shift that owns the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function shift()
    {
        return $this->belongsTo(Shift::class);
    }

    public function routeTo()
    {
        return $this->belongsTo(Route::class, 'route_from_id', 'id');
    }

    public function routeFrom()
    {
        return $this->belongsTo(Route::class, 'route_to_id', 'id');
    }

    public function employeeLeaves()
    {
        return $this->hasMany(EmployeeLeave::class, 'employee_id');
    }

    public function employeeAttendance()
    {
        return $this->hasMany(EmployeeAttendance::class, 'employee_id', 'employee_id');
    }

    public function allowances()
    {
        return $this->hasMany(EmployeeAllowance::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class)->where('status', false);
    }

    //=========================================== SCOPE QUIRES START ==============================================//

    public function scopeLineManagers($query)
    {
        $managersId = User::whereNotNull('line_manager_id')->select('employee_id')->get()->toArray();
        $query = $query->whereIn('line_manager_id', $managersId);
        return $query;
    }

    public function scopeUnsetLinemanager($query, $employees)
    {
        $query->whereIn('employee_id', $employees)->update(['line_manager_id' => null]);
        return $query;
    }

    public function scopeSetLinemanager($query, $employees, $linemanager)
    {
        $query->whereIn('employee_id', $employees)->update(['line_manager_id' => $linemanager]);
        return $query;
    }

    public function scopeSalaryEmployees($query, $paygroup, $startDate, $endDate)
    {
        $query->where('pay_group_id', $paygroup)->whereDoesntHave('employeeSalary', function ($q) use ($startDate, $endDate) {
            $q->whereDate('start_date', '<=', $startDate)->whereDate('end_date', '>=', $endDate);
        });
        return $query;
    }

    public function scopeSalaryGeneratedEmployees($query, $paygroup, $startDate, $endDate)
    {

        $query->with(['employeeSalary' => function ($query) use ($startDate, $endDate) {
            $query->whereDate('start_date', '<=', $startDate)->whereDate('end_date', '>=', $endDate);
        }, 'employeeSalary.details.allowance', 'employeeSalary.details.deduction'])
            ->where('pay_group_id', $paygroup)
            ->whereHas('employeeSalary', function ($q) use ($startDate, $endDate) {
                $q->whereDate('start_date', '<=', $startDate)->whereDate('end_date', '>=', $endDate);
            });
        return $query;
    }

    public function scopeEmployeesSalary($query, $employees, $startDate, $endDate)
    {

        $query = $query->with('employeeSalary.details.allowance', 'employeeSalary.details.deduction')
            ->whereIn('employee_id', $employees)
            ->whereHas('employeeSalary', function ($q) use ($startDate, $endDate) {
                $q->whereDate('start_date', '<=', $startDate)->whereDate('end_date', '>=', $endDate);
            });
        return $query;
    }

    public function scopeLineManagerEmployees($query, $linemanager)
    {

        $query = $query->where('line_manager_id', $linemanager);
        return $query;
    }

    //=========================================== SCOPE QUIRES END ==============================================//

    // public static function boot()
    // {
    //     parent::boot();

    //     self::updating(function ($model) {
    //         $model->full_name = $model->first_name . ' ' . $model->middle_name . ' ' . $model->last_name;
    //     });

    //     self::creating(function ($model) {
    //         $model->full_name = $model->first_name . ' ' . $model->middle_name . ' ' . $model->last_name;
    //     });
    // }
}
