<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relation extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'name',
        'description',
        'status',
        'created_by',
        'updated_by',
    ];

    public function contactPersons(){
        return $this->hasMany(ContactPerson::class, 'relation', 'id');
    }

    public function dependants(){
        return $this->hasMany(EmployeeDependant::class);
    }
}
