<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttendanceStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'is_present',
        'on_leave',
        'status',
        'created_by',
        'updated_by'
    ];
}
