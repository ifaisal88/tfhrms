<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\EmployeeSalary;
use DB;

class EmployeeSalaryDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'employee_salary_id',
        'deduction_id',
        'allowance_id',
        'amount',
        'remarks',
    ];

    public function salary()
    {
        return $this->belongsTo(EmployeeSalary::class);
    }
    public function allowance()
    {
        return $this->belongsTo(Allowance::class);
    }
    public function deduction()
    {
        return $this->belongsTo(Deduction::class);
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            DB::beginTransaction();
        });

        self::created(function ($model) {
            try {
                DB::commit();
            } catch (\Exception $e) {
                throw new \Exception('Nodel not added');
            }
        });

        self::updating(function ($model) {
            // ... code here
        });

        self::updated(function ($model) {
            // ... code here
        });

        self::deleting(function ($model) {
            // ... code here
        });

        self::deleted(function ($model) {
            // ... code here
        });
    }
}
