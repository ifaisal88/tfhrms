<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class EmployeeAsset extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'asset_type_id',
        'asset_brand_id',
        'asset_item_id',
        'serial_no',
        'asset_condition_id',
        'value',
        'currency_id',
        'issue_date',
        'return_date',
        'description',
        'return_description',
        'returned',
        'created_by',
        'updated_by',
    ];

    protected $appends = ['assigning_images', 'return_images'];

    public function employee()
    {
        return $this->belongsTo(User::class, 'user_employee_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function assetBrand()
    {
        return $this->belongsTo(AssetBrand::class);
    }

    public function assetCondition()
    {
        return $this->belongsTo(AssetCondition::class);
    }

    public function assetType()
    {
        return $this->belongsTo(AssetType::class);
    }

    public function assetItem()
    {
        return $this->belongsTo(AssetItem::class);
    }

    public function assetImages()
    {
        return $this->hasMany(EmployeeAssetImage::class);
    }

    public function getAssigningImagesAttribute()
    {
        $images = [];
        $imagesData = $this->assetImages()->where('type', 'assigning')->get(); 
        foreach ($imagesData as $image) {
            $images[] = [
                'id' => $image->id,
                'image' => $image->image,
                'path' => asset('media_assets/' . $this->id . '/'.$image->image)
            ];
        }
        return $images;
    }

    public function getReturnImagesAttribute()
    {
        $images = [];
        $imagesData = $this->assetImages()->where('type', 'return')->get(); 
        foreach ($imagesData as $image) {
            $images[] = [
                'id' => $image->id,
                'image' => $image->image,
                'path' => asset('media_assets/' . $this->id . '/'.$image->image)
            ];
        }
        return $images;
    }
}