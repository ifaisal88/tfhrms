<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetItemStock extends Model
{
    use HasFactory;

    protected $fillable = [
        'asset_item_id',
        'asset_vendor_id',
        'quantity',
        'description',
        'status',
        'attachement_file_path'
    ];

    public function item()
    {
        return $this->belongsTo(AssetItem::class, 'asset_item_id');
    }

    public function vendor()
    {
        return $this->belongsTo(AssetVendor::class, 'asset_vendor_id');
    }
}
