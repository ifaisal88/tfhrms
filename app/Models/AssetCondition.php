<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AssetCondition extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'status',
        'created_by',
        'updated_by'
    ];

    public function employeeAssets()
    {
        return $this->hasmany(EmployeeAsset::class);
    }

    public function items()
    {
        return $this->hasMany(AssetItem::class);
    }
}
