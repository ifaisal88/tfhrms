<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    use HasFactory;

    public $fillable = [
        'name',
        'description',
        'status',
        'created_by',
        'updated_by'
    ];

    public function employeeDependantBenefits()
    {
        return $this->hasMany(EmployeeDependantBenefit::class);
    }

    public function routeEmployees()
    {
        return $this->hasMany(User::class);
    }
}
