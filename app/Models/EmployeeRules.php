<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeRules extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'contract_type_id',
        'notice_period',
        'shift_id',
        'leave_per_annum_id',
        'graduity_rule_country_id',
        'ticket_entitlement_id',
        'routes_from_id',
        'routes_to_id',
        'max_fair_rates',
        'status',
        'description',
        'created_by',
        'updated_by',
    ];
}
