<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InsuranceBenefit extends Model
{
    use HasFactory;

    protected $fillable = [
        'insurance_category_id',
        'name',
        'code',
        'description',
        'percentage',
        'amount',
        'status',
    ];


    public function insuranceCategory()
    {
        return $this->belongsTo(InsuranceCategory::class);
    }
}
