<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeDocument extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'document_number',
        'document_status_id',
        'document_type_id',
        'issue_date',
        'expiry_date',
        'place_of_issue_country_id',
        'issued_by_country_id',
        'notify_period_in_days',
        'document_path',
        'description',
        'created_by',
        'updated_by',
    ];

    protected $appends = ['path'];

    public function getPathAttribute()
    {
        return asset('media_documents/' . $this->employee->employee_id .'/'.$this->document_path);
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'user_employee_id');
    }

    public function documentStatus()
    {
        return $this->belongsTo(DocumentStatus::class);
    }

    public function documentType()
    {
        return $this->belongsTo(DocumentType::class);
    }

    public function placeOfIssue()
    {
        return $this->belongsTo(Country::class, 'place_of_issue_country_id', 'id');
    }

    public function issuedBy()
    {
        return $this->belongsTo(Country::class, 'issued_by_country_id', 'id');
    }
}
