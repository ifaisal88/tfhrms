<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    use HasFactory;

    protected $fillable = [
        'module_id',
        'name',
        'description',
        'status',
        'created_by',
        'updated_by'
    ];

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
