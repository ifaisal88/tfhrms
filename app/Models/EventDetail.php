<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'event_type_id',
        'name',
        'description',
        'status',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'repeating',
        'created_by',
        'updated_by'
    ];

    public function eventType()
    {
        return $this->belongsTo(EventType::class);
    }
}
