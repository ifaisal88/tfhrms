<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class institute extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'country_id',
        'description',
        'status',
    ];

    public function educations()
    {
        return $this->hasMany(EmployeeEducation::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
