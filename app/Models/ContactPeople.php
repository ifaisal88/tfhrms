<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactPeople extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_employee_id',
        'full_name',
        'email',
        'relation',
        'mobile_number',
        'home_phone_number',
        'work_number',
        'address',
        'description',
        'created_by',
        'updated_by'
    ];

    public function employee(){
        return  $this->belongsTo(User::class, 'employee_id');
    }

    public function relationName(){
        return $this->belongsTo(Relation::class, 'relation');
    }
}
