<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeAttendance extends Model
{
    use HasFactory;

    protected $fillable = [
        'employee_id',
        'attendance_status_id',
        'attendance_date',
        'checkin_time',
        'checkout_time',
        'remarks',
        'created_by',
        'updated_by'
    ];

    protected $appends = ['total_time'];

    public function getTotalTimeAttribute()
    {
        $startDate = new Carbon($this->attendance_date.' '.$this->checkin_time);
        $endDate = new Carbon($this->attendance_date.' '.$this->checkout_time);
        return $startDate->diff($endDate)->format('%H:%I');
    }

    public function attendanceStatus()
    {
        return $this->belongsTo(AttendanceStatus::class);
    }

    public function employee()
    {
        return $this->belongsTo(User::class, 'employee_id', 'employee_id');
    }
}
