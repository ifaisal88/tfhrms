<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'status',
        'color',
        'created_by',
        'updated_by'
    ];

    public function events()
    {
        return $this->hasMany(EventDetail::class);
    }
}
