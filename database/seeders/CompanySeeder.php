<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Company;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $country = Country::where('name', 'UAE')->first();
        $companies = Company::all();
        if (count($companies)==0) {
            DB::table('companies')->insert([
                [
                    'name' => 'Company 1',
                    'country_id' => $country->id,
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Company 2',
                    'country_id' => $country->id,
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
