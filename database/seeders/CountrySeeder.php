<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $countries = Country::all();

        if ($countries->isEmpty()) {
            $sql = file_get_contents(base_path() . '/database/seeders/sql/countries.sql');
            DB::unprepared($sql);
        } else {
            $this->command->line('Already Seeded');
        }
    }
}
