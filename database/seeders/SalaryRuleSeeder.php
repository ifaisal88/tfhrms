<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\SalaryRule;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SalaryRuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $salary_rules = SalaryRule::all();
        if (count($salary_rules)==0) {
            DB::table('salary_rules')->insert([
                [
                    'name' => 'Salary Rule 1',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Salary Rule 2',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
