<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\CostCenter;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CostCenterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $costCenters = CostCenter::all();
        if (count($costCenters)==0) {
            DB::table('cost_centers')->insert([
                [
                    'name' => 'IML ES',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'IML General Trading',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
