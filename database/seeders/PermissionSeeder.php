<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::all();
        if (count($permissions) == 0) {
            DB::table('permissions')->insert([
                [
                    'name' => 'read',
                    'general' => 1,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                ],
                [
                    'name' => 'store',
                    'general' => 1,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                ],
                [
                    'name' => 'update',
                    'general' => 1,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                ],
                [
                    'name' => 'delete',
                    'general' => 1,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                ],
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
