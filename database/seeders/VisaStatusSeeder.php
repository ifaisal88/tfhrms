<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\VisaStatus;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VisaStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $visa_statuses = VisaStatus::all();
        if (count($visa_statuses)==0) {
            DB::table('visa_statuses')->insert([
                [
                    'name' => 'Perminant',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Visit',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
