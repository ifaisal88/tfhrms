<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\User::factory(10)->create();
        $this->call([
            //PositiionCategorySeeder::class,
            DesignationSeeder::class,
            CountrySeeder::class,
            RelationSeeder::class,
            DepartmentSeeder::class,
            SubDepartmentSeeder::class,
            CitySeeder::class,
            ProjectSeeder::class,
            CostCenterSeeder::class,
            ShiftSeeder::class,
            LeavePerAnumSeeder::class,
            TicketEntitlementSeeder::class,
            CurrencySeeder::class,
            AssetBrandSeeder::class,
            AssetConditionSeeder::class,
            AssetTypeSeeder::class,
            BankSeeder::class,
            CompanySeeder::class,
            // ContactPeopleSeeder::class,
            ContractTypeSeeder::class,
            DocumentTypeSeeder::class,
            DocumentStatusSeeder::class,
            EducationTypeSeeder::class,
            PositionCategorySeeder::class,
            PayGroupSeeder::class,
            SponsorSeeder::class,
            ReligionSeeder::class,
            QualificationSeeder::class,
            EmployeeTypeSeeder::class,
            VisaStatusSeeder::class,
            EmployeeCategorySeeder::class,
            InstituteSeeder::class,
            RouteSeeder::class,
            InsuranceCompanySeeder::class,
            InsuranceCategorySeeder::class,
            EmployeeStatusSeeder::class,
            AccountSeeder::class,
            ProvisionSeeder::class,
            SalaryRuleSeeder::class,
            PaymentTypeSeeder::class,
            InsuranceProviderSeeder::class,
            AllowanceSeeder::class,
            DeductionSeeder::class,
            AttendanceStatusSeeder::class,
            LeaveTypeSeeder::class,
            EmployeeLeaveSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            // PermissionRoleSeeder::class
        ]);
    }
}