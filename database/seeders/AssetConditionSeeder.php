<?php

namespace Database\Seeders;


use Carbon\Carbon;
use App\Models\User;
use App\Models\AssetCondition;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AssetConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $assetConditions = AssetCondition::all();
        if (count($assetConditions)==0) {
            DB::table('asset_conditions')->insert([
                [
                    'name' => 'Brand New',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Used',
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded!');
        }
    }
}
