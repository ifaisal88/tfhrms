<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\City;
use App\Models\Country;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user  = User::first();
        $country = Country::where('name', 'UAE')->first();
        $cities = City::all();
        if (count($cities)==0) {
            DB::table('cities')->insert([
                [
                    'name' => 'Al Ain',
                    'country_id' => $country->id,
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Abu Dhabi',
                    'country_id' => $country->id,
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Dubai',
                    'country_id' => $country->id,
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Sharjah',
                    'country_id' => $country->id,
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ],
                [
                    'name' => 'Ajman',
                    'country_id' => $country->id,
                    'description' => '',
                    'status' => true,
                    'created_at' => new Carbon(),
                    'updated_at' => new Carbon(),
                    'created_by' => $user->employee_id,
                    'updated_by' => $user->employee_id
                ]
            ]);
        } else {
            $this->command->line('Already Seeded');
        }
    }
}
