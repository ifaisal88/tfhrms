<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditEmployeeAssetsDropExtraKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_assets', function (Blueprint $table) {
            $table->dropForeign('employee_assets_asset_brand_id_foreign');
            $table->dropForeign('employee_assets_asset_type_id_foreign');
            $table->dropForeign('employee_assets_asset_condition_id_foreign');
            $table->dropForeign('employee_assets_currency_id_foreign');
            $table->unsignedBigInteger('asset_brand_id')->nullable()->change();
            $table->unsignedBigInteger('asset_type_id')->nullable()->change();
            $table->unsignedBigInteger('asset_condition_id')->nullable()->change();
            $table->unsignedBigInteger('currency_id')->nullable()->change();
            $table->integer('value')->nullable()->change();
            $table->unsignedBigInteger('asset_item_id')->after('asset_condition_id')->nullable();
            $table->foreign('asset_item_id')->references('id')->on('asset_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_assets', function (Blueprint $table) {
            $table->foreign('asset_brand_id')->references('id')->on('asset_brands');
            $table->foreign('asset_type_id')->references('id')->on('asset_types');
            $table->foreign('asset_condition_id')->references('id')->on('asset_conditions');
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->unsignedBigInteger('asset_brand_id')->nullable(false)->change();
            $table->unsignedBigInteger('asset_type_id')->nullable(false)->change();
            $table->unsignedBigInteger('asset_condition_id')->nullable(false)->change();
            $table->unsignedBigInteger('currency_id')->nullable(false)->change();
            $table->string('serial_no')->nullable(false)->change();
            $table->integer('value')->nullable(false)->change();
            $table->dropForeign('employee_assets_asset_item_id_foreign');
            $table->dropColumn('asset_item_id');
        });
    }
}
