<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeAssetImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_asset_images', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_asset_id');
            $table->string('type');
            $table->string('image');
            $table->timestamps();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();

            $table->foreign('employee_asset_id')->references('id')->on('employee_assets');
            $table->foreign('created_by')->references('employee_id')->on('users');
            $table->foreign('updated_by')->references('employee_id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_asset_images');
    }
}
