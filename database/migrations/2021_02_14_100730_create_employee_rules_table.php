<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_rules', function (Blueprint $table) {
            $table->id();
            $table->string('user_id');
            $table->string('name');
            $table->unsignedBigInteger('contract_type_id');
            $table->date('notice_period');
            $table->unsignedBigInteger('shift_id');
            $table->unsignedBigInteger('leave_per_annum_id');
            $table->unsignedBigInteger('graduity_rule_country_id');
            $table->unsignedBigInteger('ticket_entitlement_id');
            $table->unsignedBigInteger('routes_from_id');
            $table->unsignedBigInteger('routes_to_id');
            $table->float('max_fair_rates', 8, 2);
            $table->boolean('status')->default(true);
            $table->string('description')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('user_id')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('contract_type_id')
                  ->references('id')
                  ->on('contract_types');

            $table->foreign('shift_id')
                  ->references('id')
                  ->on('shifts');

            $table->foreign('leave_per_annum_id')
                  ->references('id')
                  ->on('leave_per_annums');

            $table->foreign('graduity_rule_country_id')
                  ->references('id')
                  ->on('countries');

            $table->foreign('ticket_entitlement_id')
                  ->references('id')
                  ->on('ticket_entitlements');

            $table->foreign('routes_from_id')
                  ->references('id')
                  ->on('routes');

            $table->foreign('routes_to_id')
                  ->references('id')
                  ->on('routes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_rules');
    }
}
