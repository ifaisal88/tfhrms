<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('asset_type_id');
            $table->unsignedBigInteger('asset_brand_id');
            $table->unsignedBigInteger('asset_condition_id');
            $table->unsignedBigInteger('currency_id');
            $table->string('name');
            $table->float('purchase_price');
            $table->float('sale_price');
            $table->string('description')->nullable();
            $table->boolean('status')->default(true);
            $table->timestamps();

            $table->foreign('asset_type_id')->references('id')->on('asset_types');
            $table->foreign('asset_brand_id')->references('id')->on('asset_brands');
            $table->foreign('asset_condition_id')->references('id')->on('asset_conditions');
            $table->foreign('currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_items');
    }
}
