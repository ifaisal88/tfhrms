<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeDeductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_deductions', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id')->required();
            $table->unsignedBigInteger('deduction_id')->required();
            $table->string('remarks')->nullable()->default(null);
            $table->string('type')->nullable()->default(null);
            $table->float('amount')->nullable()->default(null);
            $table->boolean('status')->nullable()->default(true);
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('employee_id')
                  ->references('employee_id')
                  ->on('users');
            
            $table->foreign('deduction_id')
                  ->references('id')
                  ->on('allowances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_deductions');
    }
}
