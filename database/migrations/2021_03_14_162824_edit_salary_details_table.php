<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditSalaryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_salary_details', function (Blueprint $table) {
            $table->dropForeign('employee_salary_details_allowance_id_foreign')->nullable()->default(null)->change();
            $table->dropForeign('employee_salary_details_deduction_id_foreign')->nullable()->default(null)->change();
            
            $table->unsignedBigInteger('allowance_id')->nullable()->default(null)->change();
            $table->unsignedBigInteger('deduction_id')->nullable()->default(null)->change();

            $table->foreign('allowance_id')
                ->references('id')
                ->on('allowances');

            $table->foreign('deduction_id')
                ->references('id')
                ->on('deductions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_salary_details', function (Blueprint $table) {
            $table->unsignedBigInteger('allowance_id')->nullable(false)->change();
            $table->unsignedBigInteger('deduction_id')->nullable(false)->change();
            $table->dropForeign('employee_salary_details_deduction_id_foreign')->nullable()->default(null)->change();
            $table->dropForeign('employee_salary_details_deduction_id_foreign')->nullable()->default(null)->change();

            $table->foreign('allowance_id')
                ->references('id')
                ->on('employee_allowances');

            $table->foreign('deduction_id')
                ->references('id')
                ->on('employee_deductions');
        });
    }
}
