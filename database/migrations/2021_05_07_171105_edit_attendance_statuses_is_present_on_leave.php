<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditAttendanceStatusesIsPresentOnLeave extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendance_statuses', function (Blueprint $table) {
            $table->boolean('is_present')->default(false)->after('status');
            $table->boolean('on_leave')->default(false)->after('is_present');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendance_statuses', function (Blueprint $table) {
            $table->dropColumn('is_present');
            $table->dropColumn('on_leave');
        });
    }
}
