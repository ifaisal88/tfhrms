<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeProvisionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_provisions', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->unsignedBigInteger('provision_id');
            $table->string('description')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('user_employee_id')
                  ->references('employee_id')
                  ->on('users');
            
            $table->foreign('provision_id')
                  ->references('id')
                  ->on('provisions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_provisions');
    }
}
