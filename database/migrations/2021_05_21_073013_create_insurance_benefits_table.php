<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceBenefitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_benefits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('insurance_category_id')->unsigned();
            $table->string('name');
            $table->string('code');
            $table->string('description')->nullable()->default(null);
            $table->float('percentage')->nullable()->default(null);
            $table->float('amount')->nullable()->default(null);
            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_benefits');
    }
}
