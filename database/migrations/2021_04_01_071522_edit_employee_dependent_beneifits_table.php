<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditEmployeeDependentBeneifitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_dependant_benefits', function (Blueprint $table) {
            $table->boolean('return_ticket')->default(false)->after('fare_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_dependant_benefits', function (Blueprint $table) {
            $table->dropColumn('return_ticket');
        });
    }
}
