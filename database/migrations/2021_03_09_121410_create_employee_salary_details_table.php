<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeSalaryDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_salary_details', function (Blueprint $table) {
            $table->id();
            $table->string('employee_id')->required();
            $table->unsignedBigInteger('employee_salary_id')->required();
            $table->unsignedBigInteger('deduction_id')->required();
            $table->unsignedBigInteger('allowance_id')->required();
            $table->float('amount')->required();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('employee_id')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('employee_salary_id')
                  ->references('id')
                  ->on('employee_salaries');

            $table->foreign('deduction_id')
                  ->references('id')
                  ->on('employee_deductions');

            $table->foreign('allowance_id')
                ->references('id')
                ->on('employee_allowances');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_salary_details');
    }
}
