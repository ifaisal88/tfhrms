<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_documents', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->string('document_number');
            $table->unsignedBigInteger('document_status_id');
            $table->unsignedBigInteger('document_type_id');
            $table->date('issue_date');
            $table->date('expiry_date');
            $table->unsignedBigInteger('place_of_issue_country_id');
            $table->unsignedBigInteger('issued_by_country_id');
            $table->smallInteger('notify_period_in_days');
            $table->string('document_path');
            $table->string('description')->nullable()->default(null);
            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('user_employee_id')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('document_status_id')
                  ->references('id')
                  ->on('document_statuses');

            $table->foreign('document_type_id')
                  ->references('id')
                  ->on('document_types');

            $table->foreign('place_of_issue_country_id')
                  ->references('id')
                  ->on('countries');

            $table->foreign('issued_by_country_id')
                  ->references('id')
                  ->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_documents');
    }
}
