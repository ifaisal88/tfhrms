<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeLeaveStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_leave_statuses', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_leave_id')->nullable()->default(null);
            $table->boolean('status')->required()->default(null);
            $table->string('pending_with_user_id')->nullable()->default(null);
            $table->string('approved_by_user_id')->nullable()->default(null);
            $table->date('submitted_date');
            $table->date('approved_date')->nullable()->default(null);
            $table->string('comments')->nullable()->default(null);
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('employee_leave_id')
                ->references('id')
                ->on('employee_leaves');

            $table->foreign('pending_with_user_id')
                ->references('employee_id')
                ->on('users');

            $table->foreign('approved_by_user_id')
                ->references('employee_id')
                ->on('users');

            $table->foreign('created_by')
                ->references('employee_id')
                ->on('users');

            $table->foreign('updated_by')
                ->references('employee_id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_leave_statuses');
    }
}
