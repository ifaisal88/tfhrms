<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_insurances', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->unsignedBigInteger('insurance_provider_id');
            $table->unsignedBigInteger('insurance_company_id');
            $table->string('policy_name');
            $table->unsignedBigInteger('insurance_category_id');
            $table->date('employee_insured_start_date');
            $table->date('employee_insured_end_date');
            $table->float('annual_premium_per_year_rate', 8, 2);
            $table->float('actual_rate', 8, 2);
            $table->float('monthly_rate', 8, 2);
            $table->string('description')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('user_employee_id')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('insurance_provider_id')
                  ->references('id')
                  ->on('insurance_providers');

            $table->foreign('insurance_company_id')
                  ->references('id')
                  ->on('insurance_companies');

            $table->foreign('insurance_category_id')
                  ->references('id')
                  ->on('insurance_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_insurances');
    }
}
