<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetItemStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_item_stocks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('asset_item_id');
            $table->unsignedBigInteger('asset_vendor_id');
            $table->integer('quantity');
            $table->boolean('status')->default(true);
            $table->string('description')->nullable();
            $table->string('attachement_file_path')->nullable();
            $table->timestamps();

            $table->foreign('asset_item_id')->references('id')->on('asset_items');
            $table->foreign('asset_vendor_id')->references('id')->on('asset_vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_item_stocks');
    }
}
