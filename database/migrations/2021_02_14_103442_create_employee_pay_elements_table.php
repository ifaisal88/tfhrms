<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeePayElementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_pay_elements', function (Blueprint $table) {
            $table->id();
            $table->string('user_employee_id');
            $table->string('salary_rule');
            $table->unsignedBigInteger('currency_id');
            $table->float('basic_salary', 8, 2);
            $table->float('house_allounce', 8, 2);
            $table->float('transport_allounce', 8, 2);
            $table->float('gross_salary', 8, 2);
            $table->dateTime('last_salary_revision_date');
            $table->unsignedBigInteger('pay_group_id');
            $table->unsignedBigInteger('account_id');
            $table->string('description')->nullable()->default(null);
            $table->timestamps();
            $table->string('created_by')->nullable()->default(null);
            $table->string('updated_by')->nullable()->default(null);

            $table->foreign('created_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('updated_by')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('user_employee_id')
                  ->references('employee_id')
                  ->on('users');

            $table->foreign('pay_group_id')
                  ->references('id')
                  ->on('pay_groups');

            $table->foreign('account_id')
                  ->references('id')
                  ->on('accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_pay_elements');
    }
}
