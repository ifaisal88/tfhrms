<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditEmployeeAttendancesNullableCheckinCheckout extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_attendances', function (Blueprint $table) {
            // $table->renameColumn('remkarks', 'remarks');
            $table->time('checkin_time')->nullable()->change();
            $table->time('checkout_time')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_attendances', function (Blueprint $table) {
            $table->renameColumn('remarks', 'remkarks');
            $table->time('checkin_time')->nullable(false)->change();
            $table->time('checkout_time')->nullable(false)->change();
        });
    }
}
